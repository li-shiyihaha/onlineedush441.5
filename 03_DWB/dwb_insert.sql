 --动态分区配置
set hive.exec.dynamic.partition=true;
set hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;
--hive压缩
set hive.exec.compress.intermediate=true;
set hive.exec.compress.output=true;
--写入时压缩生效
set hive.exec.orc.compression.strategy=COMPRESSION;

insert into oe_dwb.dwb_leave_detail partition (dt)
select
        fl.id,
        fl.class_id,
        fl.student_id,
        fl.leave_type,
        fl.audit_state,
        fl.begin_time,
        fl.begin_time_type,
        fl.end_time,
        fl.end_time_type,
        fl.valid_state,
        fl.cancel_state,
        du.class_date,
        du.content,
        dc.morning_begin_time,
        dc.morning_end_time,
        dc.afternoon_begin_time,
        dc.afternoon_end_time,
        dc.evening_begin_time,
        dc.evening_end_time,
        fl.dt
from oe_dwd.fac_leave fl
left join oe_dwd.dim_upload du on fl.class_id=du.class_id
left join oe_dwd.dim_class dc on fl.class_id=dc.class_id
;

insert  into  table oe_dwb.dwb_signin_detail partition (dt)
select
        fs.id,
        fs.time_table_id,
        fs.class_id,
        fs.student_id,
        fs.signin_time,
        fs.signin_date,
        du.class_date,
        du.content,
        dc.morning_begin_time,
        dc.morning_end_time,
        dc.afternoon_begin_time,
        dc.afternoon_end_time,
        dc.evening_begin_time,
        dc.evening_end_time,
        fs.dt
from oe_dwd.fac_signin fs
left join oe_dwd.dim_class dc on fs.class_id=dc.class_id
left join oe_dwd.dim_upload du on fs.class_id=du.class_id
where if(content = '',null,content) is not null;
;




