------------------因为采用了动态分区插入技术 因此需要设置相关参数---------------
--分区
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;
--hive压缩
set hive.exec.compress.intermediate=true;
set hive.exec.compress.output=true;
--写入时压缩生效
set hive.exec.orc.compression.strategy=COMPRESSION;


-- dwb 访问咨询信息宽表
insert into table oe_dwb.dwb_web_chat_ems_2019_07 partition(dt)
select
    t.id                                   ,
    t.session_id                           ,
    t.sid                                  ,
    substring(t.create_time,1,10) as create_time,
    t.seo_source                           ,
    t.ip                                   ,
    t.area                                 ,
    t.country                              ,
    t.origin_channel                       ,
    t.msg_count                            ,
    t1.from_url                            ,
    t.start_date as dt
       --substring (create_time)
     from oe_dwd.dwd_fact_web_chat_ems_2019_07 t
left join
          oe_dwd.dwd_dim_web_chat_text_ems_2019_07 t1
on t.id = t1.id ;

