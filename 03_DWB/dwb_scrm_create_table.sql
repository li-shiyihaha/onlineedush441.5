create database if not exists oe_dwb;
use oe_dwb;

-- 意向宽表
drop table if exists oe_dwb.customer_relationship_detail;
create table oe_dwb.customer_relationship_detail
(
    customer_relationship_id string comment '意向id',
    customer_id              string comment '学员id',
    customer_name            string comment '姓名',       --关联客户静态信息表
    origin_type              string comment '数据来源',     --确定线上线下
    origin_channel           string comment '数据来源渠道',   --确认数据来源渠道
    clue_state               string comment '线索状态',     --判断新老学员
    school_id                string comment '校区id',
    school_name              string comment '校区名',
    subject_id               string comment '学科id',     --关联学科表
    subject_name             string comment '学科名',      --插入时可name as subject_name
    creator                  string comment '创建人，员工id', --关联员工表
    creator_name             string comment '创建人姓名',
    tdepart_id               string comment '直属部门',     --关联部门表
    tdepart_name             string comment '部门名称',     --部门名称，即咨询中心名称,插入时，name as tdepart_name
    country                  string comment '国家',       --从客户静态信息表中插入用 if 数组area_array判断输出
    province                 string comment '省份',       --从客户静态信息表中插入用 if 数组area_array判断输出
    city                     string comment '城市',       --从客户静态信息表中插入用 if 数组area_array判断输出
    dt                       string comment '导入时间'
) comment '意向宽表'
    partitioned by (create_date_time string)
    row format delimited fields terminated by '\001';     ---用默认的文本格式和hive默认的压缩方式来降低性能要求

-- 线索宽表
drop table if exists oe_dwb.customer_clue_detail;
create table oe_dwb.customer_clue_detail
(
    clue_id                  string comment '线索id',
    customer_relationship_id string comment '意向id',         --插入时，id as customer_relationship_id
    origin_type              string comment '数据来源',         --确定线上线下
    valid                    string comment '该线索是否是网资有效线索', --判断有效
    clue_state               string comment '线索状态',         --判断新老学员
    is_repeat                string comment '是否重复线索(手机号维度) 0:正常 1：重复',
    appeal_status            string comment '申诉状态，0:待稽核 1:无效 2：有效',
    hour                     string comment '每小时',
    dt                       string comment '导入时间'
) comment '线索宽表'
    partitioned by (create_date_time string)
    row format delimited fields terminated by '\t';