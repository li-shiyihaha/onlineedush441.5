---- 注意，如果session无法选中presto数据源----------
---- 把sql文件语法方言支持调整为Generic sql模式----------
/*insert into hive.oe_dws.dws_sale_daycount
with tmp as (
);*/

-- 学生出勤状态信息表
/* todo 涉及的表: dwb_signin_detail
        涉及的字段：维度字段：class_date,class_id
                 指标字段：sign_time 打卡时间 student_id
                 指标计算：先判断是否出勤，打卡时间 between 上午课前40分钟和课程结束-->出勤
                         时间处理：关联到在读日期和上课日期，确定在同一天。之后对时分秒进行截取，time-interval‘40’minute就是时间计算减去40分钟
                         如果出勤了，对其进行1的计数，如果大于1，那么继续判断。如果sum小于1，判断为其他，标记2.
                        继续判断：出勤中打卡的时间在开课前40分钟到开课后10分钟，判断出勤，标记1，否则哦判断迟到，标记2
         源头数据过滤： 所有出勤记录中的content（上课内容）必须为正常课程，
                        1需要过滤掉content 中为开班典礼和空字段数据。ifnull(col,'') != '' 相当于 col !=null and col != ''

                        2打卡信息中都开启公屏
*/
use oe_dws;
-- 出勤人数统计（先对同一天同一个班级同于一个学生，的上午，中午，下午打卡数据做判断）
insert into oe_dws.dws_signin_daycount
with t1 as (
select
       dsd.id,
       dsd.time_table_id,
       dsd.signin_time,
       dsd.signin_date,
       dsd.share_state,
       dsd.content,
       dsd.morning_begin_time,
       dsd.morning_end_time,
       dsd.afternoon_begin_time,
       dsd.afternoon_end_time,
       dsd.evening_begin_time,
       dsd.evening_end_time,
       dsd.dt,
       dsd.class_date as class_date,
       dsd.class_id as class_id,
       dsd.student_id as student_id,
       dsd.content,
      if(  --根据签到时间判断签到的人是正常出勤还是迟到
          sum(
              case when cast(substring (signin_time ,12 ,8)as time)  between  cast(morning_begin_time as time ) - interval '40'minute
              and cast(morning_end_time as time ) /* 课前四十分钟到上课结束时间内签到算正常出勤 返回 0*/
              then 1
              else 0 end
              ) >0,
          if(
              sum(
                  case when cast(substring (signin_time ,12 ,8)as time)  between  cast(morning_begin_time as time ) - interval '40'minute
              and cast(morning_begin_time as time ) +interval '10'minute -- 上课开始后十分钟到课程结束之间签到算迟到 返回 1
                  then 1
                  else 0 end
                  ) >0,0,1
              ), 2 --   2 则代表为在时间内签到 包含请假和旷课
           ) as  att_morning,
       if(
           sum(
               case when cast(substring (signin_time, 12 ,8) as time )between  cast(afternoon_begin_time as time ) - interval '40'minute
               and cast(afternoon_end_time as time )
               then 1
               else 0 end
               ) >0,
           if(
               sum(
                  case when cast(substring (signin_time ,12 ,8)as time)  between  cast(afternoon_begin_time as time ) - interval '40'minute
              and cast(afternoon_begin_time as time ) +interval '10'minute
                  then 1
                  else 0 end
                  ) >0,0,1
               ),2
           ) as att_afternoon,
       if(
           sum(
               case when cast(substring (signin_time, 12 ,8) as time )between  cast(evening_begin_time as time ) - interval '40'minute
               and cast(evening_end_time as time )
               then 1
               else 0 end
               ) >0,
           if(
               sum(
                  case when cast(substring (signin_time ,12 ,8)as time)  between  cast(evening_begin_time as time ) - interval '40'minute
              and cast(evening_begin_time as time ) +interval '10'minute
                  then 1
                  else 0 end
                  ) >0,0,1
               ),2
           ) as att_evening
from (select * from oe_dwb.dwb_signin_detail where share_state = '1' and content !='开班典礼' and content !='' and content is not null) dsd
join oe_dwd.dim_stu_cnt sc on dsd.class_id=sc.class_id and dsd.signin_date=sc.studying_date
group by dsd.id, dsd.time_table_id, dsd.signin_time, dsd.signin_date, dsd.share_state, dsd.content, dsd.morning_begin_time, dsd.morning_end_time, dsd.afternoon_begin_time, dsd.afternoon_end_time, dsd.evening_begin_time, dsd.evening_end_time, dsd.dt, dsd.class_date, dsd.class_id, dsd.student_id, dsd.content),
-- 分一下标记重复
t2 as (
select
       *,
       row_number()over(partition by class_date,class_id,student_id order by class_date,class_id,student_id desc) rn1,
       row_number()over(partition by class_date,class_id,student_id order by class_date,class_id,student_id desc) rn2,
       row_number()over(partition by class_date,class_id,student_id order by class_date,class_id,student_id desc) rn3
from t1),
-- 去重操作
t3 as (
select *
from t2 where rn1 = 1 and rn2= 1 and rn3 =1),

-- count 操作形成上午中午下午的迟到和出勤人数 (count存疑)
t4 as (
select
       class_date,
       class_id,
       count(if (att_morning in (0,1),student_id,null)) as att_morning_cnt,
       count(if (att_morning in (1),student_id,null)) as late_morning_cnt,
       count(if (att_afternoon in (0,1),student_id,null)) as att_afternoon_cnt,
       count(if (att_afternoon in (1),student_id,null)) as late_afternoon_cnt,
       count(if (att_evening in (0,1),student_id,null)) as att_evening_cnt,
       count(if (att_evening in (1),student_id,null)) as late_evening_cnt
from t2
where rn1 = 1 and rn2= 1 and rn3 =1
group by class_date,class_id
     ),


/* todo 请假主题  算出每天每个班级的请假人数
        涉及到的表：请假主题宽表dwb_leave_detail，
        维度字段：class_date,class_id
        指标字段：student_id 也就是学员id
        过滤条件：上课内容不为空，不是开班典礼,审核通过，
        计算思路：判断请假的有效性，
               1.请假有效的前提1--有效的作息时间内请假
               2.请假有效的前提2--在有有效排课的时间内请假
                   请假仅在有排课的时候请假才算请假。class_date>=begin_time and class_date<=end_time
*/
-- 上午请假申请记录
 t5 as (
    select
      class_date,
      class_id,
      count(student_id) as leave_morning_cnt
   from (select * from oe_dwb.dwb_leave_detail
                     where content !='开班典礼'
                     and content !=''
                     and content is not null --上课得有内容才算请假
                     and audit_state = 1     --通过审核才算请假
                     and cancel_state =0     --未被撤销状态才算
                     and valid_state = 1     --有效请假才算请假
                     and class_date between use_begin_date  and  use_end_date --（请假开始日期，上课，请假结束）才算请假
                     and concat(class_date,' ',morning_begin_time) >= begin_time --（请假开始日期，早上上课，请假结束）算早上请假
                     and concat(class_date,' ',morning_begin_time) <= end_time    )
    group by class_date,class_id
   ) ,

-- 下午请假申请记录
t6 as (
    select
      class_date,
      class_id,
      count(student_id) as leave_afternoon_cnt
   from (select * from oe_dwb.dwb_leave_detail
                     where content !='开班典礼'
                     and content !=''
                     and content is not null --上课得有内容才算请假
                     and audit_state = 1     --通过审核才算请假
                     and cancel_state =0     --未被撤销状态才算
                     and valid_state = 1     --有效请假才算请假
                     and class_date between use_begin_date  and  use_end_date --（请假开始日期，上课，请假结束）才算请假
                     and concat(class_date,' ',afternoon_begin_time) >= begin_time --（请假开始日期，早上上课，请假结束）算早上请假
                     and concat(class_date,' ',afternoon_begin_time) <= end_time )
    group by class_date,class_id
   ) ,

-- 晚上请假申请记录
 t7 as (
    select

      class_date,
      class_id,
      count(student_id) as leave_evening_cnt
   from (select * from oe_dwb.dwb_leave_detail
                     where content !='开班典礼'
                     and content !=''
                     and content is not null --上课得有内容才算请假
                     and audit_state = 1     --通过审核才算请假
                     and cancel_state =0     --未被撤销状态才算
                     and valid_state = 1     --有效请假才算请假
                     and class_date between use_begin_date  and  use_end_date --（请假开始日期，上课，请假结束）才算请假
                     and concat(class_date,' ',evening_begin_time) >= begin_time --（请假开始日期，早上上课，请假结束）算早上请假
                     and concat(class_date,' ',evening_begin_time) <= end_time )
    group by class_date,class_id
   ),

 --三个请假的信息联合起来
t8 as  (
select
        coalesce (t5.class_date,t6.class_date,t7.class_date) as class_date,
        coalesce (t5.class_id ,t6.class_id,t7.class_id)as class_id,
        leave_morning_cnt,
        leave_afternoon_cnt,
        leave_evening_cnt
        from t5 full join t6 on t5.class_date=t6.class_date and t5.class_id =t6.class_id
                  full join t7 on t7.class_date=t5.class_date and t7.class_id =t5.class_id ),

-- 请假和出勤（迟到）的信息联合起来，再和班级总人数联合起来。
t9 as (
select
       coalesce (t4.class_date,t8.class_date) as class_date,
       coalesce (t4.class_id,t8.class_date) as class_id,
       stu_cnt,
       att_morning_cnt,
       late_morning_cnt,
       att_afternoon_cnt,
       late_afternoon_cnt,
       att_evening_cnt,
       late_evening_cnt,
       leave_morning_cnt,
       leave_afternoon_cnt,
       leave_evening_cnt,
       (cast(stu_cnt as int) - cast(t4.att_morning_cnt as int)-cast(t8.leave_morning_cnt as int))
           as absent_morning_cnt,
       (cast(stu_cnt as int) - cast(t4.att_afternoon_cnt as int)-cast(t8.leave_afternoon_cnt as int))
           as absent_afternoon_cnt,
       (cast(stu_cnt as int) - cast(t4.att_evening_cnt as int)- cast(t8.leave_evening_cnt as int))
           as absent_evening_cnt
from t4 full join t8  on   t4.class_date=t8.class_date and t4.class_id =t8.class_id
    join oe_dwd.dim_stu_cnt dsc on dsc.class_id=t4.class_id and dsc.studying_date=t4.class_date
    )

--和 在读学员人数信息表尽心关联获取班级人数信息。(内连接join获取有效的在读班级，关联条件班级id相同)
select
       class_date,
       t9.class_id as class_id,
       stu_cnt,
       'time_type' as time_type,
       att_morning_cnt,
    cast(att_morning_cnt as decimal(8,2))/cast(stu_cnt as decimal (8,2)) as att_morning_rate,
       att_afternoon_cnt,
    cast(att_afternoon_cnt as decimal(8,2))/cast(stu_cnt as decimal (8,2)) as att_afternoon_rate,
       att_evening_cnt,
    cast(att_evening_cnt as decimal(8,2))/cast(stu_cnt as decimal (8,2)) as att_evening_rate,
       late_morning_cnt,
    cast(late_morning_cnt as decimal(8,2))/cast(stu_cnt as decimal (8,2)) as late_morning_rate,
       late_afternoon_cnt,
    cast(late_afternoon_cnt as decimal(8,2))/cast(stu_cnt as decimal (8,2)) as late_afternoon_rate,
       late_evening_cnt,
    cast(late_evening_cnt as decimal(8,2))/cast(stu_cnt as decimal (8,2)) as late_evening_rate,
       leave_morning_cnt,
    cast(leave_morning_cnt as decimal(8,2))/cast(stu_cnt as decimal (8,2)) as leave_morning_rate,
       leave_afternoon_cnt,
    cast(leave_afternoon_cnt as decimal(8,2))/cast(stu_cnt as decimal (8,2)) as leave_afternoon_rate,
       leave_evening_cnt,
    cast(leave_evening_cnt as decimal(8,2))/cast(stu_cnt as decimal (8,2)) as leave_evening_rate,
       absent_morning_cnt,    -- '上午旷课人数'
    cast(absent_morning_cnt as decimal(8,2))/cast(stu_cnt as decimal (8,2)) as absent_morning_rate,
       absent_afternoon_cnt, -- '下午旷课人数'
    cast(absent_afternoon_cnt as decimal(8,2))/cast(stu_cnt as decimal (8,2)) as absent_afternoon_rate,
       absent_evening_cnt, --- '晚上旷课人数'
    cast(absent_evening_cnt as decimal(8,2))/cast(stu_cnt as decimal (8,2)) as absent_evening_rate
from t9 ;

