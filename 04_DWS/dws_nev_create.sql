--dws  daily每天
create database if not exists oe_dws;
use oe_dws;
drop table if exists oe_dws.dws_web_chat_ems_2019_07_daycount;
create table  oe_dws.dws_web_chat_ems_2019_07_daycount
(   date_code                    string          comment '会话创建时间',
    area                         string          comment 'area',
    country                      string          comment '所在国家',
    seo_source                   string          comment '搜索来源',
    origin_channel               string          comment '来源渠道(广告)',
    from_url                     string          comment '来源页面',
    group_type                   string          comment '分组标识',
    sid_cnt                     bigint         comment '访问用户量',
    ip_cnt                      bigint         comment 'ip地址个数',
    session_cnt                 bigint         comment '会话个数',
    msg_num                     bigint         comment '咨询人数 '
)
comment '访问咨询信息表'
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');