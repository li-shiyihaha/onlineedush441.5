#! /bin/bash
export LANG=zh_CN.UTF-8
HIVE_HOME=/usr/bin/hive


${HIVE_HOME} -S -e "
-- 赵长浩
create database if not exists oe_dws;
use oe_dws;
-- 建表(HIVE中执行建表语句）
drop table if exists oe_dws.dws_application_day_cnt;
create table if not exists oe_dws.dws_application_day_cnt(
    id                    string            comment '意向id',
    origin_type           string            comment '线上线下',
    itcast_school_id      string            comment '校区id',
    itcast_school_name    string            comment '校区名称',
    itcast_subject_id     string            comment '学科id',
    itcast_subject_name   string            comment '学科名称',
    origin_channel        string            comment '线索来源渠道',
    consulting_center     string            comment '咨询中心',
    group_type            string            comment '分组类型：origin_type,itcast_school,itcast_subject,origin_channel,consulting_center',
    application_cnt       bigint            comment '报名人数'
)partitioned by(dt string)
row format delimited fields terminated by '\t'
stored as TextFile;
------------------------------------------------------------------------
insert into hive.oe_dws.dws_application_day_cnt
with tmp as (select
    id,
    origin_type,
    payment_state,
    itcast_school_id,
    itcast_school_name,
    itcast_subject_id,
    itcast_subject_name,
    origin_channel,
    name as consulting_center,
    row_number() over (partition by id) as rn,
    dt
from hive.oe_dwb.dwb_customer_relationship_wide)
select
    max(id) as id,
    origin_type,
    itcast_school_id,
    itcast_school_name,
    itcast_subject_id,
    itcast_subject_name,
    origin_channel,
    consulting_center,
    case when grouping(consulting_center) = 0  then 'consulting_center'
         when grouping(origin_channel) = 0  then 'origin_channel'
         when grouping(itcast_school_id,itcast_school_name,itcast_school_id,itcast_school_name) = 0 then 'school_subject'
         when grouping(itcast_subject_id,itcast_subject_name) = 0 then 'itcast_subject'
         when grouping(itcast_school_id,itcast_school_name) = 0 then 'itcast_school'
         when grouping(origin_type) = 0  then  'origin_type'
         when grouping(dt) = 0  then 'all'
         else 'other'  end as group_type,
    case grouping(dt,origin_type,itcast_school_id,itcast_school_name,itcast_subject_id,itcast_subject_name,origin_channel,consulting_center)
         when  79    then count(if (rn =1 and payment_state='PAID',1,null))
         when  63    then count(if (rn =1 and payment_state='PAID',1,null))
         when  15    then count(if (rn =1 and payment_state='PAID',1,null))
         when  51    then count(if (rn =1 and payment_state='PAID',1,null))
         when  3     then count(if (rn =1 and payment_state='PAID',1,null))
         when  61    then count(if (rn =1 and payment_state='PAID',1,null))
         when  62    then count(if (rn =1 and payment_state='PAID',1,null))
         else null end  as application_cnt,
    dt
from tmp
group by
grouping sets (dt,
              (dt,origin_type),
              (dt,origin_type,itcast_school_id,itcast_school_name),
              (dt,origin_type,itcast_subject_id,itcast_subject_name),
              (dt,origin_type,itcast_school_id,itcast_school_name,itcast_subject_id,itcast_subject_name),
              (dt,origin_type,origin_channel),
              (dt,origin_type,consulting_center))
;
"


