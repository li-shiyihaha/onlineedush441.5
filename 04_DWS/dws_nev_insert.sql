insert into hive.oe_dws.dws_web_chat_ems_2019_07_daycount
with wb_tmp as (
    select session_id,
           sid,
           ip,
           seo_source,
           country,
           area,
           origin_channel,
           msg_count,
           from_url,
           create_time as date_code,


             row_number()  over (partition by sid, session_id)                    as ses_rn,
             row_number() over (partition by  ip )                                 as ip_rn,


              row_number() over (partition by sid)                                  as sid_rn,
              row_number() over (partition by sid,from_url)                         as url_rn,
              row_number() over (partition by sid,origin_channel )                  as channel_rn,
              row_number() over (partition by sid,seo_source)                       as source_rn,
              row_number() over (partition by sid,area   )                          as area_rn,

              row_number() over (partition by ip,from_url)                          as ip_url_rn,
              row_number() over (partition by ip,origin_channel )                   as ip_channel_rn,
              row_number() over (partition by ip,seo_source)                        as ip_source_rn,
              row_number() over (partition by ip,area   )                           as ip_area_rn,

              row_number() over (partition by sid,session_id,from_url)              as ses_url_rn,
              row_number() over (partition by sid,session_id,origin_channel )       as ses_channel_rn,
              row_number() over (partition by sid,session_id,seo_source)            as ses_source_rn,
              row_number() over (partition by sid,session_id,area   )               as ses_area_rn

    from oe_dwb.dwb_web_chat_ems_2019_07
)
select

    date_code,
    area,
    country,
    seo_source,
    origin_channel,
    from_url,
    --指标字段 如下
    --分组标志 group_type
    case
        when grouping(date_code, from_url) = 0 then '来源页面'
        when grouping(date_code, seo_source) = 0 then '搜索来源'
        when grouping(date_code, origin_channel) = 0 then '来源渠道'
        when grouping(date_code, area) = 0 then 'area'
        when grouping(date_code, area, country) = 0 then 'country'
        when grouping(date_code) = 0 then 'all'
        else 'other' end as group_type,
    case
        when grouping(date_code, from_url) = 0       then sum(if(url_rn = 1 and sid is not null,       1, 0))
        when grouping(date_code, seo_source) = 0     then sum(if(source_rn = 1 and sid is not null,    1, 0))
        when grouping(date_code, origin_channel) = 0 then sum(if(channel_rn = 1 and sid is not null,   1, 0))
        when grouping(date_code, area) = 0           then sum(if(area_rn = 1 and sid is not null,      1, 0))
        when grouping(date_code) = 0                 then sum(if(sid_rn = 1 and sid is not null,       1, 0))
        else null end    as sid_cnt,
    --每天  <全国各个地区area /每个来源渠道origin_channel/搜索来源seo_sourcesum from_url >   总访问IP个数
    case
        when grouping(date_code, from_url) = 0       then sum(if(ip_url_rn = 1 and ip is not null,     1, 0))
        when grouping(date_code, seo_source) = 0     then sum(if(ip_source_rn = 1 and ip is not null,  1, 0))
        when grouping(date_code, origin_channel) = 0 then sum(if(ip_channel_rn = 1 and ip is not null, 1, 0))
        when grouping(date_code, area) = 0           then sum(if(ip_area_rn = 1 and ip is not null,        1, 0))
        when grouping(date_code) = 0                 then sum(if(ip_rn = 1 and ip is not null,         1, 0))
        else null end    as ip_cnt,
    ---每天  <全国各个地区area /每个来源渠道origin_channel/搜索来源seo_sourc面 from_url >    总访问Session个数
    case
        when grouping(date_code, from_url) = 0       then sum(if(ses_url_rn = 1 and session_id is not null,     1, 0))
        when grouping(date_code, seo_source) = 0     then sum(if(ses_source_rn = 1 and session_id is not null,  1, 0))
        when grouping(date_code, origin_channel) = 0 then sum(if(ses_channel_rn = 1 and session_id is not null, 1, 0))
        when grouping(date_code, area) = 0           then sum(if(ses_area_rn = 1 and session_id is not null,    1, 0))
        when grouping(date_code) = 0                 then sum(if(ses_rn = 1 and session_id is not null,         1, 0))
        else null end    as session_cnt,
    --msg_num
     case
         when grouping(date_code, area) = 0
              then sum(if(sid_rn = 1 and sid is not null and msg_count is not null and msg_count > 0, 1, 0)) --as session_area_cnt,
         else 0 end    as msg_num

from wb_tmp
where country = '中国'
group by
    grouping sets
    (
  --天
  (date_code),
      (date_code, area),
      (date_code, area, country),
      (date_code, from_url),
      (date_code, seo_source),
      (date_code, origin_channel)
    );
