with t1 as (
select
      substr(dsd.class_date,1,4) as t_year,
      substr(dsd.class_date,6,2) as t_mon,
      substr(dsd.class_date,9,2) as t_day,
      dsd.class_date as class_date,
      dsd.class_id as class_id,
      dsd.student_id as student_id,
       sc.stu_cnt as stu_cnt,
       dsd.signin_date as signin_date,
    content,
      if(
          sum(
              case when cast(substring (signin_time ,12 ,8)as time)  between  cast(morning_begin_time as time ) - interval '40'minute
              and cast(morning_end_time as time )
              then 1
              else 0 end
              ) >0,
          if(
              sum(
                  case when cast(substring (signin_time ,12 ,8)as time)  between  cast(morning_begin_time as time ) - interval '40'minute
              and cast(morning_begin_time as time ) +interval '10'minute
                  then 1
                  else 0 end
                  ) >0,0,1
              ), 2
           ) as  att_morning,
       if(
           sum(
               case when cast(substring (signin_time, 12 ,8) as time )between  cast(afternoon_begin_time as time ) - interval '40'minute
               and cast(afternoon_end_time as time )
               then 1
               else 0 end
               ) >0,
           if(
               sum(
                  case when cast(substring (signin_time ,12 ,8)as time)  between  cast(afternoon_begin_time as time ) - interval '40'minute
              and cast(afternoon_begin_time as time ) +interval '10'minute
                  then 1
                  else 0 end
                  ) >0,0,1
               ),2
           ) as att_afternoon,
       if(
           sum(
               case when cast(substring (signin_time, 12 ,8) as time )between  cast(evening_begin_time as time ) - interval '40'minute
               and cast(evening_end_time as time )
               then 1
               else 0 end
               ) >0,
           if(
               sum(
                  case when cast(substring (signin_time ,12 ,8)as time)  between  cast(evening_begin_time as time ) - interval '40'minute
              and cast(evening_begin_time as time ) +interval '10'minute
                  then 1
                  else 0 end
                  ) >0,0,1
               ),2
           ) as att_evening
from (select * from oe_dwb.dwb_signin_detail where share_state = '1' and content !='开班典礼' and content !='' and content is not null) dsd
join oe_dwd.dim_stu_cnt sc on dsd.class_id=sc.class_id and dsd.signin_date=sc.studying_date
group by dsd.class_date,dsd.class_id, dsd.student_id,content,sc.stu_cnt,dsd.signin_date),
-- 分一下标记重复
t2 as (
select t_year,
       t_mon,
       t_day,
       class_date,
       class_id,
       student_id,
       att_morning,
       att_afternoon,
       att_evening,
       stu_cnt,
       signin_date,
       row_number()over(partition by t_year,t_mon,t_day, class_date,class_id,student_id order by class_date,class_id,student_id desc) rn1,
       row_number()over(partition by t_year,t_mon,t_day, class_date,class_id,student_id order by class_date,class_id,student_id desc) rn2,
       row_number()over(partition by t_year,t_mon,t_day, class_date,class_id,student_id order by class_date,class_id,student_id desc) rn3
from t1),
-- 去重操作
t3 as (
select *
from t2 where rn1 = 1 and rn2= 1 and rn3 =1),

-- count 操作
t4 as (
select t_year,
       t_mon,
       t_day,
       class_date,
       class_id,
       stu_cnt,
       signin_date,
       count(if (att_morning in (0,1),student_id,null)) as att_morning,
       count(if (att_morning in (1),student_id,null)) as late_morning,
       count(if (att_afternoon in (0,1),student_id,null)) as att_afternoon,
       count(if (att_afternoon in (1),student_id,null)) as late_afternoon,
       count(if (att_evening in (0,1),student_id,null)) as att_evening,
       count(if (att_evening in (1),student_id,null)) as late_evening
from t2 group by t_year, t_mon, t_day,class_date,class_id ,stu_cnt,signin_date),
     t5 as (
select
       *,
(att_morning+att_afternoon+att_evening) as att_cnt,
(late_afternoon+late_evening+late_morning) as late_cnt
from t4
)
select
class_date,
       t5.class_id,
       t5.stu_cnt,
       t5.signin_date,
       cast(att_cnt as decimal (8,2))/cast(dst.stu_cnt as decimal (8,2)) as att_rate

from t5 join oe_dwd.dim_stu_cnt dst on dst.class_id=t5.class_id and t5.class_date=dst.studying_date
;