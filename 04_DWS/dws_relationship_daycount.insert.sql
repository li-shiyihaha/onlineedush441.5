---- 意向主题日统计
insert into hive.oe_dws.relationship_daycount
with temp as (select
-- 判断
case when origin_type = 'NETSERVICE' or origin_type = 'PRESIGNUP' then 'online'
else 'offline' end as origin_type,    --数据来源，线上线下
case when clue_state = 'VALID_NEW_CLUES' then 'new'
else 'old' end as clue_state,     --新老学员
origin_channel, --数据来源渠道
--统计计算
customer_relationship_id,  --意向id
--维度字段
create_date_time, --数据创建时间
country,        --国家
province,       --省份
city,           --城市
subject_id,
subject_name,   --学科
school_id,
school_name,    --校区
tdepart_id,
tdepart_name,   --部门
row_number() over (partition by customer_relationship_id) as rn
from hive.oe_dwb.customer_relationship_detail)
select
       create_date_time,
       case when origin_type = 'online'  then 'online'
       else 'offline' end as origin_type,
       case when clue_state = 'new' then 'new'
       else 'old' end as clue_state,
       country,
       province,
       city,
       subject_id,
       subject_name,
       school_id,
       school_name,
       origin_channel,
       tdepart_id,
       tdepart_name,
--group_type
   case  when grouping(country)=0
       then 'country'
     when grouping (province)=0
       then 'province'
     when grouping (city)=0
        then 'city'
     when grouping (subject_id ,subject_name)=0
        then 'subject'
     when grouping(school_id,school_name)=0
        then 'school'
     when grouping(origin_channel) =0
        then 'origin_channel'
     when grouping(tdepart_id,tdepart_name)=0
        then 'tdepart'
        else 'all' end as group_type,
--意向用户个数
       case
           when grouping(city) = 0
               then count(if(rn = 1,customer_relationship_id, null))
           when grouping(province) = 0
               then count(if(rn = 1,customer_relationship_id, null))
           when grouping(country) = 0
               then count(if(rn = 1,customer_relationship_id, null))
           when grouping(subject_id, subject_name) = 0
               then count(if(rn = 1,customer_relationship_id, null))
           when grouping(school_id, school_name) = 0
               then count(if(rn = 1,customer_relationship_id, null))
           when grouping(origin_channel) = 0
               then count(if(rn = 1,customer_relationship_id, null))
           when grouping(tdepart_id, tdepart_name) = 0
               then count(if(rn = 1,customer_relationship_id, null))
           when grouping(origin_type, clue_state) = 0
               then count(if(rn = 1,customer_relationship_id, null))
           else 0 end as relationship_cnt
from temp
group by
grouping sets (
(create_date_time,origin_type,clue_state),
(create_date_time,origin_type,clue_state,country),
(create_date_time,origin_type,clue_state,province),
(create_date_time,origin_type,clue_state,city),
(create_date_time,origin_type,clue_state,subject_id ,subject_name),
(create_date_time,origin_type,clue_state,school_id,school_name),
(create_date_time,origin_type,clue_state,origin_channel),
(create_date_time,origin_type,clue_state,tdepart_id,tdepart_name)
);
