--rpt  访问咨询信息报表

--需求1：统计每天/每月/每个季度/每年总访问用户量、总访问IP个数、总访问Session个数**
insert into hive.oe_rpt.rpt_web_chat_ems_2019_07_daycnt
--每天 总访问用户量、总访问IP个数、总访问Session个数
select
       sid_cnt,
       ip_cnt,
       session_cnt,
       year_code,
       quarter_code ,
       month_code,
       date_code
from oe_dm.dm_web_chat_ems_2019_07
where group_type_new = 'all' and time_type = '天'
order by year_code,quarter_code,month_code,date_code;

--每个月总访问用户量、总访问IP个数、总访问Session个数
insert into  hive.oe_rpt.rpt_web_chat_ems_2019_07_monthcnt
select
sid_cnt,
ip_cnt,
session_cnt,
month_code
from oe_dm.dm_web_chat_ems_2019_07
where group_type_new = 'all' and time_type = '月';

--每个季度/每年总访问用户量、总访问IP个数、总访问Session个数
insert into hive.oe_rpt.rpt_web_chat_ems_2019_07_quartercnt

select
sid_cnt,
ip_cnt,
session_cnt,
quarter_code
from oe_dm.dm_web_chat_ems_2019_07
where group_type_new = 'all' and time_type = '季度';

--每年总访问用户量、总访问IP个数、总访问Session个数
insert into hive.oe_rpt.rpt_web_chat_ems_2019_07_yearcnt
select
sid_cnt,
ip_cnt,
session_cnt,
year_code
from oe_dm.dm_web_chat_ems_2019_07
where group_type_new = 'all' and time_type = '年';


--需求2：每天/每月/每个季度/每年全国各个地区访问用户量、访问IP个数、访问Session个数 咨询率

--每天全国各个地区访问用户量、访问IP个数、访问Session个数 咨询率
insert into hive.oe_rpt.rpt_web_chat_ems_2019_07_cnt_area_day
select
sid_cnt,
ip_cnt,
session_cnt,
year_code,
quarter_code,
month_code,
date_code,
if(sid_cnt =0 , null,cast (msg_num as decimal(38,4)) / sid_cnt  * 100 ) as msg_rate
from oe_dm.dm_web_chat_ems_2019_07
where group_type_new = 'area'  and time_type = '天';


insert into hive.oe_rpt.rpt_web_chat_ems_2019_07_cnt_area_month
--每月 各个地区访问用户量、访问IP个数、访问Session个数 咨询率
select
sid_cnt,
ip_cnt,
session_cnt,
month_code,
if(sid_cnt =0 , null,cast (msg_num as decimal(38,4)) / sid_cnt  * 100 ) as msg_rate
from oe_dm.dm_web_chat_ems_2019_07
where group_type_new = 'area' and time_type = '月';

--每个季度 各个地区访问用户量、访问IP个数、访问Session个数 咨询率
insert into hive.oe_rpt.rpt_web_chat_ems_2019_07_cnt_area_quarter
select
sid_cnt,
ip_cnt,
session_cnt,
quarter_code,
if(sid_cnt =0 , null, cast (msg_num as decimal(38,4)) / sid_cnt  * 100 ) as msg_rate
from oe_dm.dm_web_chat_ems_2019_07
where group_type_new = 'area' and time_type = '季度';

--每年   各地区访问用户量、访问IP个数、访问Session个数 咨询率
insert into hive.oe_rpt.rpt_web_chat_ems_2019_07_cnt_area_year
select
sid_cnt,
ip_cnt,
session_cnt,
year_code,
if(sid_cnt =0 , null, cast (msg_num as decimal(38,4)) / sid_cnt  * 100 ) as msg_rate
from oe_dm.dm_web_chat_ems_2019_07
where group_type_new = 'area' and time_type = '年' ;


--需求3：每天/每月/每个季度/每年每个来源渠道的访问用户量、访问IP个数、访问Session个数**
-- rpt_web_chat_ems_2019_07_cnt_channel

--每天每个来源渠道的访问用户量、访问IP个数、访问Session个数
insert into hive.oe_rpt.rpt_web_chat_ems_2019_07_cnt_channel_day
select
      sid_cnt,
      ip_cnt,
      session_cnt,
      year_code,
      quarter_code,
      month_code,
      date_code
from oe_dm.dm_web_chat_ems_2019_07
where group_type_new = '来源渠道'  and time_type = '天'

--每月 来源渠道访问用户量、访问IP个数、访问Session个数
insert into hive.oe_rpt.  rpt_web_chat_ems_2019_07_cnt_channel_month
select
      sid_cnt,
      ip_cnt,
      session_cnt,
      month_code
from oe_dm.dm_web_chat_ems_2019_07
where group_type_new = '来源渠道'  and time_type = '月'

--每个季度 来源渠道访问用户量、访问IP个数、访问Session个数
insert into  hive.oe_rpt. rpt_web_chat_ems_2019_07_cnt_channel_quarter
select
      sid_cnt,
      ip_cnt,
      session_cnt,
      quarter_code
from oe_dm.dm_web_chat_ems_2019_07
where group_type_new = '来源渠道'  and time_type = '季度';

--每年 来源渠道 访问用户量、访问IP个数、访问Session个数
insert into hive.oe_rpt.rpt_web_chat_ems_2019_07_cnt_channel_year
select
      sid_cnt,
      ip_cnt,
      session_cnt,
      year_code
from oe_dm.dm_web_chat_ems_2019_07
where group_type_new = '来源渠道'  and time_type = '年' ;



--需求4：每天/每月/每个季度/每年每个搜索来源的访问用户量、访问IP个数、访问Session个数
-- rpt_web_chat_ems_2019_07_cnt_source

--每天每个搜索来源的访问用户量、访问IP个数、访问Session个数
insert into  hive.oe_rpt. rpt_web_chat_ems_2019_07_cnt_source_day
select
sid_cnt,
ip_cnt,
session_cnt,
year_code,
quarter_code,
month_code,
date_code
 from oe_dm.dm_web_chat_ems_2019_07
where  group_type_new = '搜索来源'  and time_type = '天';


--每月/每个季度/每年每个搜索来源的访问用户量、访问IP个数、访问Session个数
insert into hive.oe_rpt. rpt_web_chat_ems_2019_07_cnt_source_month
select
sid_cnt,
ip_cnt,
session_cnt,
month_code
 from oe_dm.dm_web_chat_ems_2019_07
where  group_type_new = '搜索来源'  and time_type = '月';

--每个季度/每年每个搜索来源的访问用户量、访问IP个数、访问Session个数
insert into  hive.oe_rpt.rpt_web_chat_ems_2019_07_cnt_source_quarter
select
sid_cnt,
ip_cnt,
session_cnt,
quarter_code
 from oe_dm.dm_web_chat_ems_2019_07
where  group_type_new = '搜索来源'  and time_type = '季度';

--每年每个搜索来源的访问用户量、访问IP个数、访问Session个数
insert into hive.oe_rpt.  rpt_web_chat_ems_2019_07_cnt_source_year
select
sid_cnt,
ip_cnt,
session_cnt,
year_code
 from oe_dm.dm_web_chat_ems_2019_07
where  group_type_new = '搜索来源'  and time_type = '年';


--需求5：每天/每月/每个季度/每年每个来源页面的访问用户量、访问IP个数、访问Session个数**

-- rpt_web_chat_ems_2019_07_url_day
insert into hive.oe_rpt. rpt_web_chat_ems_2019_07_cnt_url_day
select
sid_cnt,
ip_cnt,
session_cnt,
year_code,
quarter_code,
month_code,
date_code
 from oe_dm.dm_web_chat_ems_2019_07
where
group_type_new = '来源页面'  and time_type = '天';

--rpt_web_chat_ems_2019_07_url_month
insert into  hive.oe_rpt.rpt_web_chat_ems_2019_07_cnt_url_month
select
sid_cnt,
ip_cnt,
session_cnt,
month_code
 from oe_dm.dm_web_chat_ems_2019_07
where
group_type_new = '来源页面'  and time_type = '月';

--rpt_web_chat_ems_2019_07_url_quarter
insert into hive.oe_rpt.rpt_web_chat_ems_2019_07_cnt_url_quarter
select
sid_cnt,
ip_cnt,
session_cnt,
quarter_code
 from oe_dm.dm_web_chat_ems_2019_07
where
group_type_new = '来源页面'  and time_type = '季度';


--rpt_web_chat_ems_2019_07_url_year
insert into  hive.oe_rpt.rpt_web_chat_ems_2019_07_cnt_url_year
select
sid_cnt,
ip_cnt,
session_cnt,
year_code
 from oe_dm.dm_web_chat_ems_2019_07
where
group_type_new = '来源页面'  and time_type = '年';







