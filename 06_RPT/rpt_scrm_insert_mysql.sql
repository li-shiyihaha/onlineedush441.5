create database if not exists oe_olap character set utf8;
use oe_olap;

-- 每天/每月/每年线上线下以及新老学员的意向用户个数
create table mysql.oe_olap.rpt_relationship_all
as
select
    date_time,
    year_code,
    month_code,
    day_month_num,
    dim_date_id,
    origin_type,
    clue_state,
    time_type,
    relationship_cnt
from hive.oe_dm.dm_relationship
where time_type!='other'
and group_type='all';

-- 每天/每月/每年各地区的线上线下以及新老学员的意向用户个数

create table mysql.oe_olap.rpt_relationship_country
as
select
    date_time,
    year_code,
    month_code,
    day_month_num,
    dim_date_id,
    origin_type,
    clue_state,
    country,
    province,
    city,
    relationship_cnt
from hive.oe_dm.dm_relationship
where time_type != 'other'
and group_type='country'
and country='中国';

create table mysql.oe_olap.rpt_relationship_province
as
select
    date_time,
    year_code,
    month_code,
    day_month_num,
    dim_date_id,
    origin_type,
    clue_state,
    country,
    province,
    city,
    relationship_cnt
from hive.oe_dm.dm_relationship
where time_type != 'other'
and group_type='province'
and province is not null ;

create table mysql.oe_olap.rpt_relationship_city
as
select
    date_time,
    year_code,
    month_code,
    day_month_num,
    dim_date_id,
    origin_type,
    clue_state,
    country,
    province,
    city,
    relationship_cnt
from hive.oe_dm.dm_relationship
where time_type != 'other'
and group_type='city'
and city is not null ;

-- 每天/每月/每年各学科线上线下以及新老学员的意向用户个数Top10
create table mysql.oe_olap.rpt_relationship_subject
as
with tmp as (select
    date_time,
    year_code,
    month_code,
    day_month_num,
    dim_date_id,
    origin_type,clue_state,
    subject_id,subject_name,
    relationship_cnt,
    row_number() over(partition by time_type,origin_type,clue_state,subject_id order by relationship_cnt desc) as rn
from hive.oe_dm.dm_relationship
where time_type!='other'
and group_type='subject'
and subject_id is not null)
select *
from tmp
where rn<=10;


-- 每天/每月/每年各校区线上线下以及新老学员的意向用户个数Top10
create table mysql.oe_olap.rpt_relationship_school
as
with tmp as (select
    date_time,
    year_code,
    month_code,
    day_month_num,
    dim_date_id,
    origin_type,clue_state,
    school_id,school_name,
    relationship_cnt,
    row_number() over(partition by time_type,origin_type,clue_state,school_id order by relationship_cnt desc) as rn
from hive.oe_dm.dm_relationship
where time_type!='other'
and group_type='school'
and school_id is not null)
select *
from tmp
where rn<=10;

-- 每天/每月/每年各来源渠道线上线下以及新老学员的意向用户个数
create table mysql.oe_olap.rpt_relationship_origin
as
select
    date_time,
    year_code,
    month_code,
    day_month_num,
    dim_date_id,
    origin_type,
    clue_state,
    origin_channel,
    relationship_cnt
from hive.oe_dm.dm_relationship
where time_type!='other'
and group_type='origin';
-- 每天/每月/每年各咨询中心线上线下以及新老学员的意向用户个数
create table mysql.oe_olap.rpt_relationship_tdepart
as
select
    date_time,
    year_code,
    month_code,
    day_month_num,
    dim_date_id,
    origin_type,
    tdepart_id,
    tdepart_name,
    relationship_cnt
from hive.oe_dm.dm_relationship
where time_type!='other'
and group_type='tdepart'
and tdepart_id is not null ;
-- 每天线上线下及新老学员的有效线索个数
create table mysql.oe_olap.rpt_clue_day
as
select
    create_date_time,
    origin_type,
    clue_state,
    clu_cnt
from hive.oe_dws.clue_daycount
where group_type='day';

-- 每小时线上线下及新老学员的有效线索转化率 = 有效线索个数 / 总线索个数
create table mysql.oe_olap.rpt_clue_rate
as
select
    hour,
    origin_type,
    clue_state,
    clu_rate
from hive.oe_dws.clue_daycount
where group_type='hour';