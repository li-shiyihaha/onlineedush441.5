--rpt
create database if not exists oe_rpt;
use oe_rpt;
--1
create table oe_rpt.rpt_web_chat_ems_2019_07_daycnt
(
    sid_cnt              bigint            comment '访问用户量',
    ip_cnt               bigint            comment 'ip地址个数',
    session_cnt          bigint            comment '会话个数',
    year_code            string            comment '年',
    quarter_code         string            comment '季度',
    month_code           string            comment '月',
    date_code            string            comment '天'
)comment '访问咨询信息报表'
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

drop   table oe_rpt.rpt_web_chat_ems_2019_07_monthcnt;
create table oe_rpt.rpt_web_chat_ems_2019_07_monthcnt
(
    sid_cnt              bigint            comment '访问用户量',
    ip_cnt               bigint            comment 'ip地址个数',
    session_cnt          bigint            comment '会话个数',
    month_code           string            comment '月'
)comment '访问咨询信息报表'
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

drop table if exists  oe_rpt.rpt_web_chat_ems_2019_07_quartercnt;
create table oe_rpt.rpt_web_chat_ems_2019_07_quartercnt
(
    sid_cnt              bigint            comment '访问用户量',
    ip_cnt               bigint            comment 'ip地址个数',
    session_cnt          bigint            comment '会话个数',
    quarter_code         string            comment '季度'
)comment '访问咨询信息报表'
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

drop table if exists oe_rpt.rpt_web_chat_ems_2019_07_yearcnt;
create table oe_rpt.rpt_web_chat_ems_2019_07_yearcnt
(
    sid_cnt              bigint            comment '访问用户量',
    ip_cnt               bigint            comment 'ip地址个数',
    session_cnt          bigint            comment '会话个数',
    year_code            string            comment '年'
)comment '访问咨询信息报表'
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');



--2
create table oe_rpt.rpt_web_chat_ems_2019_07_cnt_area_day
(

    sid_cnt              bigint            comment '访问用户量',
    ip_cnt               bigint            comment 'ip地址个数',
    session_cnt          bigint            comment '会话个数',
    year_code            string            comment '年',
    quarter_code         string            comment '季度',
    month_code           string            comment '月',
    date_code            string            comment '天',
    msg_rate            decimal(38,2)      comment '咨询率'
)comment '访问咨询信息报表'
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

drop table oe_rpt.rpt_web_chat_ems_2019_07_cnt_area_month;
create table oe_rpt.rpt_web_chat_ems_2019_07_cnt_area_month
(

    sid_cnt              bigint            comment '访问用户量',
    ip_cnt               bigint            comment 'ip地址个数',
    session_cnt          bigint            comment '会话个数',
    month_code           string            comment '月',
    msg_rate            decimal(38,2)      comment '咨询率'
)comment '访问咨询信息报表'
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');



create table oe_rpt.rpt_web_chat_ems_2019_07_cnt_area_quarter
(

    sid_cnt              bigint            comment '访问用户量',
    ip_cnt               bigint            comment 'ip地址个数',
    session_cnt          bigint            comment '会话个数',
    quarter_code         string            comment '季度',
    msg_rate            decimal(38,2)      comment '咨询率'
)comment '访问咨询信息报表'
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

create table oe_rpt.rpt_web_chat_ems_2019_07_cnt_area_year
(

    sid_cnt              bigint            comment '访问用户量',
    ip_cnt               bigint            comment 'ip地址个数',
    session_cnt          bigint            comment '会话个数',
    year_code            string            comment '年',
    msg_rate            decimal(38,2)      comment '咨询率'
)comment '访问咨询信息报表'
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

--3
create table oe_rpt.rpt_web_chat_ems_2019_07_cnt_channel_day
(
    sid_cnt              bigint            comment '访问用户量',
    ip_cnt               bigint            comment 'ip地址个数',
    session_cnt          bigint            comment '会话个数',
    year_code            string            comment '年',
    quarter_code         string            comment '季度',
    month_code           string            comment '月',
    date_code            string            comment '天'
)comment '访问咨询信息报表'
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

 create table  oe_rpt.rpt_web_chat_ems_2019_07_cnt_channel_month
(
    sid_cnt              bigint            comment '访问用户量',
    ip_cnt               bigint            comment 'ip地址个数',
    session_cnt          bigint            comment '会话个数',
    month_code           string            comment '月'
)comment '访问咨询信息报表'
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

drop table   oe_rpt.rpt_web_chat_ems_2019_07_cnt_channel_quarter;
 create table  oe_rpt.rpt_web_chat_ems_2019_07_cnt_channel_quarter
(
    sid_cnt              bigint            comment '访问用户量',
    ip_cnt               bigint            comment 'ip地址个数',
    session_cnt          bigint            comment '会话个数',
    quarter_code         string            comment '季度'
)comment '访问咨询信息报表'
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

drop table  oe_rpt.rpt_web_chat_ems_2019_07_cnt_channel_year;
 create table  oe_rpt.rpt_web_chat_ems_2019_07_cnt_channel_year
(
    sid_cnt              bigint            comment '访问用户量',
    ip_cnt               bigint            comment 'ip地址个数',
    session_cnt          bigint            comment '会话个数',
    year_code            string            comment '年'
)comment '访问咨询信息报表'
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');






--4
drop table oe_rpt.rpt_web_chat_ems_2019_07_cnt_source_day;
 create table oe_rpt.rpt_web_chat_ems_2019_07_cnt_source_day
(
    sid_cnt              bigint            comment '访问用户量',
    ip_cnt               bigint            comment 'ip地址个数',
    session_cnt          bigint            comment '会话个数',
    year_code            string            comment '年',
    quarter_code         string            comment '季度',
    month_code           string            comment '月',
    date_code            string            comment '天'
)comment '访问咨询信息报表'
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

drop table oe_rpt.rpt_web_chat_ems_2019_07_cnt_source_month;
create table  oe_rpt.rpt_web_chat_ems_2019_07_cnt_source_month
(
    sid_cnt              bigint            comment '访问用户量',
    ip_cnt               bigint            comment 'ip地址个数',
    session_cnt          bigint            comment '会话个数',
    month_code           string            comment '月'
)comment '访问咨询信息报表'
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

 create table oe_rpt.rpt_web_chat_ems_2019_07_cnt_source_quarter
(
    sid_cnt              bigint            comment '访问用户量',
    ip_cnt               bigint            comment 'ip地址个数',
    session_cnt          bigint            comment '会话个数',
    quarter_code         string            comment '季度'
)comment '访问咨询信息报表'
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');


 create table oe_rpt.rpt_web_chat_ems_2019_07_cnt_source_year
(
    sid_cnt              bigint            comment '访问用户量',
    ip_cnt               bigint            comment 'ip地址个数',
    session_cnt          bigint            comment '会话个数',
    year_code            string            comment '年'
)comment '访问咨询信息报表'
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');




--5
create table  rpt_web_chat_ems_2019_07_cnt_url_day
(
    sid_cnt              bigint            comment '访问用户量',
    ip_cnt               bigint            comment 'ip地址个数',
    session_cnt          bigint            comment '会话个数',
    year_code            string            comment '年',
    quarter_code         string            comment '季度',
    month_code           string            comment '月',
    date_code            string            comment '天'
)comment '访问咨询信息报表'
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

 create table rpt_web_chat_ems_2019_07_cnt_url_month
(
    sid_cnt              bigint            comment '访问用户量',
    ip_cnt               bigint            comment 'ip地址个数',
    session_cnt          bigint            comment '会话个数',
    month_code           string            comment '月'
)comment '访问咨询信息报表'
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');

 create table rpt_web_chat_ems_2019_07_cnt_url_quarter
(
    sid_cnt              bigint            comment '访问用户量',
    ip_cnt               bigint            comment 'ip地址个数',
    session_cnt          bigint            comment '会话个数',
    quarter_code         string            comment '季度'

)comment '访问咨询信息报表'
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');


 create table rpt_web_chat_ems_2019_07_cnt_url_year
(
    sid_cnt              bigint            comment '访问用户量',
    ip_cnt               bigint            comment 'ip地址个数',
    session_cnt          bigint            comment '会话个数',
    year_code            string            comment '年'
)comment '访问咨询信息报表'
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');




-----------------------------
--转到mysql。oe_olap

--在mysql 建库 create database if not exists oe_olap;

---area
create table mysql.oe_olap.rpt_web_chat_ems_2019_07_cnt_area_day
as
select sid_cnt,
       ip_cnt,
       session_cnt,
       year_code,
    case when quarter_code = '第二季度' then 'secondquarter'
        else null end as quarter_code,
       month_code,
       date_code,
       msg_rate
from hive.oe_rpt.rpt_web_chat_ems_2019_07_cnt_area_day;

--daycnt
create table mysql.oe_olap.rpt_web_chat_ems_2019_07_daycnt
as
select
      sid_cnt,
      ip_cnt,
      session_cnt,
      year_code,
      case when quarter_code = '第二季度' then 'secondquarter'
     else null end as quarter_code,
      month_code
      date_code

from hive.oe_rpt.rpt_web_chat_ems_2019_07_daycnt;



--source
create table mysql.oe_olap.rpt_web_chat_ems_2019_07_cnt_source_day
as
select
    sid_cnt       ,
    ip_cnt        ,
    session_cnt   ,
    year_code     ,
	 case when quarter_code = '第二季度' then 'secondquarter'
     else null end as quarter_code,
	month_code    ,
	date_code
from hive.oe_rpt.rpt_web_chat_ems_2019_07_cnt_source_day;


--channel
create table mysql.oe_olap.rpt_web_chat_ems_2019_07_cnt_channel_day
as
select
    sid_cnt       ,
    ip_cnt        ,
    session_cnt   ,
    year_code     ,
    case when quarter_code = '第二季度' then 'secondquarter'
    else null end as quarter_code,
	month_code    ,
	date_code
from hive.oe_rpt.rpt_web_chat_ems_2019_07_cnt_channel_day;


--url
create table mysql.oe_olap.rpt_web_chat_ems_2019_07_cnt_url_day
as
select
      sid_cnt       ,
      ip_cnt        ,
      session_cnt   ,
      year_code     ,
     case when quarter_code = '第二季度' then 'secondquarter'
          else null end as quarter_code,
	  month_code    ,
	  date_code
from hive.oe_rpt.rpt_web_chat_ems_2019_07_cnt_url_day;







