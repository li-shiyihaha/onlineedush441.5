-- 赵长浩
create database if not exists oe_rpt;
use oe_rpt;

--------------------------------------------------------------------------
-- 建表
drop table if exists oe_rpt.rpt_school_cnt;
create table if not exists oe_rpt.rpt_school_cnt(
  itcast_school_name  string  ,
  school_cnt          bigint
)row format delimited fields terminated by '\t'
stored as TextFile;
drop table if exists oe_rpt.rpt_subject_cnt;
create table if not exists oe_rpt.rpt_subject_cnt(
  itcast_subject_name  string  ,
  subject_cnt          bigint
)row format delimited fields terminated by '\t'
stored as TextFile;
drop table if exists oe_rpt.rpt_total_cnt;
create table if not exists oe_rpt.rpt_total_cnt(
  total_cnt          bigint
)row format delimited fields terminated by '\t'
stored as TextFile;
drop table if exists oe_rpt.rpt_online_cnt;
create table if not exists oe_rpt.rpt_online_cnt(
  online_cnt          bigint
)row format delimited fields terminated by '\t'
stored as TextFile;
drop table if exists oe_rpt.rpt_total_rate;
create table if not exists oe_rpt.rpt_total_rate(
  total_rate          decimal(38,2)
)row format delimited fields terminated by '\t'
stored as TextFile;
drop table if exists oe_rpt.rpt_online_rate;
create table if not exists oe_rpt.rpt_online_rate(
  online_rate          decimal(38,2)
)row format delimited fields terminated by '\t'
stored as TextFile;
drop table if exists oe_rpt.rpt_day_cnt;
create table if not exists oe_rpt.rpt_day_cnt(
  day_code         string  ,
  day_cnt          bigint
)row format delimited fields terminated by '\t'
stored as TextFile;
drop table if exists oe_rpt.rpt_channel_rate;
create table if not exists oe_rpt.rpt_channel_rate(
    origin_channel        string   ,
    channel_rate          decimal(38,2)
)row format delimited fields terminated by '\t'
stored as TextFile;
drop table if exists oe_rpt.rpt_center_rate;
create table if not exists oe_rpt.rpt_center_rate(
  name                string  ,
  center_rate         decimal(38,2)
)row format delimited fields terminated by '\t'
stored as TextFile;
drop table if exists oe_rpt.rpt_sch_sub_cnt;
create table if not exists oe_rpt.rpt_sch_sub_cnt(
  itcast_school_name   string  ,
  itcast_subject_name  string  ,
  sch_sub_cnt          bigint
)row format delimited fields terminated by '\t'
stored as TextFile;
