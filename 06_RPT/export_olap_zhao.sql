-- 赵长浩
create table mysql.oe_olap.rpt_school_cnt as
select  * from hive.oe_rpt.rpt_school_cnt;

create table mysql.oe_olap.rpt_subject_cnt as
select  * from hive.oe_rpt.rpt_subject_cnt;

create table mysql.oe_olap.rpt_total_cnt as
select  * from hive.oe_rpt.rpt_total_cnt;

create table mysql.oe_olap.rpt_online_cnt as
select  * from hive.oe_rpt.rpt_online_cnt;

create table mysql.oe_olap.rpt_total_rate as
select  * from hive.oe_rpt.rpt_total_rate;

create table mysql.oe_olap.rpt_online_rate as
select  * from hive.oe_rpt.rpt_online_rate;

create table mysql.oe_olap.rpt_day_cnt as
select  * from hive.oe_rpt.rpt_day_cnt;

create table mysql.oe_olap.rpt_channel_rate as
select  * from hive.oe_rpt.rpt_channel_rate;

create table mysql.oe_olap.rpt_center_rate as
select  * from hive.oe_rpt.rpt_center_rate;

create table mysql.oe_olap.rpt_sch_sub_cnt as
select  * from hive.oe_rpt.rpt_sch_sub_cnt;