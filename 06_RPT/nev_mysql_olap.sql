--rpt  导入 mysql。oe_olap库
create database if not exists oe_olap character set utf8;


---area
create table mysql.oe_olap.rpt_web_chat_ems_2019_07_cnt_area_day
as
select sid_cnt,
       ip_cnt,
       session_cnt,
       year_code,
    case when quarter_code = '第二季度' then 'secondquarter'
        else null end as quarter_code,
       month_code,
       date_code,
       msg_rate
from hive.oe_rpt.rpt_web_chat_ems_2019_07_cnt_area_day;

--daycnt

drop table mysql.oe_olap.rpt_web_chat_ems_2019_07_daycnt;
create table mysql.oe_olap.rpt_web_chat_ems_2019_07_day
as
select
      sid_cnt,
      ip_cnt,
      session_cnt,
      year_code,
      case when quarter_code = '第二季度' then 'secondquarter'
     else null end as quarter_code,
      month_code,
      date_code
from hive.oe_rpt.rpt_web_chat_ems_2019_07_daycnt;



--source
create table mysql.oe_olap.rpt_web_chat_ems_2019_07_cnt_source_day
as
select
    sid_cnt       ,
    ip_cnt        ,
    session_cnt   ,
    year_code     ,
	 case when quarter_code = '第二季度' then 'secondquarter'
     else null end as quarter_code,
	month_code    ,
	date_code
from hive.oe_rpt.rpt_web_chat_ems_2019_07_cnt_source_day;


--channel
create table mysql.oe_olap.rpt_web_chat_ems_2019_07_cnt_channel_day
as
select
    sid_cnt       ,
    ip_cnt        ,
    session_cnt   ,
    year_code     ,
    case when quarter_code = '第二季度' then 'secondquarter'
    else null end as quarter_code,
	month_code    ,
	date_code
from hive.oe_rpt.rpt_web_chat_ems_2019_07_cnt_channel_day;


--url
create table mysql.oe_olap.rpt_web_chat_ems_2019_07_cnt_url_day
as
select
      sid_cnt       ,
      ip_cnt        ,
      session_cnt   ,
      year_code     ,
     case when quarter_code = '第二季度' then 'secondquarter'
          else null end as quarter_code,
	  month_code    ,
	  date_code
from hive.oe_rpt.rpt_web_chat_ems_2019_07_cnt_url_day;
