-- 赵长浩
create database if not exists oe_dm;
use oe_dm;
-- 建表
drop table if exists oe_dm.dm_application_rollup;
create table if not exists oe_dm.dm_application_rollup(
    time_type    string            comment '时间类型',
    year_code    string            comment '年,如2014',
    year_month   string            comment '年月,如201401',
    month_code   string            comment '月份,如01',
    group_type_new   string        comment '分组类型',
    origin_type      string        comment '线上线下',
    itcast_school_id    string     comment '校区id',
    itcast_school_name  string     comment '校区名称',
    itcast_subject_id   string     comment '学科id',
    itcast_subject_name string     comment '学科名称',
    origin_channel      string     comment '线索来源渠道',
    consulting_center   string     comment '咨询中心',
    application_cnt      bigint     comment '报名人数'
)partitioned by(dt string)
row format delimited fields terminated by '\t'
stored as TextFile;
-----------------------------------------------------------------------------------------


