

insert into oe_dm.dm_final_daycount
with t1 as (
    select class_date,
           class_id,
           stu_cnt,
           time_type,
           att_morning_cnt,
           att_morning_rate,
           att_afternoon_cnt,
           att_afternoon_rate,
           att_evening_cnt,
           att_evening_rate,
           late_morning_cnt,
           late_morning_rate,
           late_afternoon_cnt,
           late_afternoon_rate,
           late_evening_cnt,
           late_evening_rate,
           coalesce (leave_morning_cnt,0)as leave_morning_cnt,
           coalesce (leave_morning_rate,0)as leave_morning_rate,
           coalesce (leave_afternoon_cnt,0)as leave_afternoon_cnt,
           coalesce (leave_afternoon_rate,0)as leave_afternoon_rate,
           coalesce (leave_evening_cnt,0)as leave_evening_cnt,
           coalesce (leave_evening_rate,0)as leave_evening_rate,
           coalesce (absent_morning_cnt,0)as absent_morning_cnt,
           coalesce (absent_morning_rate,0)as absent_morning_rate,
           coalesce (absent_afternoon_cnt,0) as absent_afternoon_cnt,
           coalesce (absent_afternoon_rate,0 ) as absent_afternoon_rate,
           coalesce (absent_evening_cnt,0) as absent_evening_cnt,
           coalesce (absent_evening_rate,0) as absent_evening_rate
    from oe_dws.dws_signin_daycount
)
select *
from t1
;

select * from oe_dws.dws_signin_daycount;