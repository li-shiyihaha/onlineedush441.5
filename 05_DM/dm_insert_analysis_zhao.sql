-- 赵长浩

delete from hive.oe_dm.dm_application_rollup;
insert into hive.oe_dm.dm_application_rollup
with tem as (select
     id,
     substring (dt,1,4) as year_code,
     substring (dt,1,7) as year_month,
     substring (dt,6,2) as month_code,
     origin_type,
     itcast_school_id,
     itcast_school_name,
     itcast_subject_id,
     itcast_subject_name,
     origin_channel,
     consulting_center,
     group_type,
     application_cnt,
     dt
from hive.oe_dws.dws_application_day_cnt),
     tmp as (select
     case when grouping(dt) = 0 then 'date'
          when grouping(year_month) = 0 then 'month'
          when grouping(year_code) = 0 then 'year'
          else 'other' end as time_type,
     year_code,
     year_month,
     group_type as group_type_old,
     case when grouping(consulting_center) = 0  then 'consulting_center'
          when grouping(origin_channel) = 0  then 'origin_channel'
          when grouping(itcast_school_id,itcast_school_name,itcast_school_id,itcast_school_name) = 0 then 'school_subject'
          when grouping(itcast_subject_id,itcast_subject_name) = 0 then 'itcast_subject'
          when grouping(itcast_school_id,itcast_school_name) = 0 then 'itcast_school'
          when grouping(origin_type) = 0  then  'origin_type'
          else 'all'  end as group_type_new,
      origin_type,
      itcast_school_id,
      itcast_school_name,
      itcast_subject_id,
      itcast_subject_name,
      origin_channel,
      consulting_center,
      group_type,
      sum(application_cnt) as application_cnt,
      dt
from tem
group by
grouping sets (
               (year_code,group_type),
              (year_code,group_type,origin_type),
              (year_code,group_type,origin_type,itcast_school_id,itcast_school_name),
              (year_code,group_type,origin_type,itcast_subject_id,itcast_subject_name),
              (year_code,group_type,origin_type,itcast_school_id,itcast_school_name,itcast_subject_id,itcast_subject_name),
              (year_code,group_type,origin_type,origin_channel),
              (year_code,group_type,origin_type,consulting_center),
               (year_month,group_type),
              (year_month,group_type,origin_type),
              (year_month,group_type,origin_type,itcast_school_id,itcast_school_name),
              (year_month,group_type,origin_type,itcast_subject_id,itcast_subject_name),
              (year_month,group_type,origin_type,itcast_school_id,itcast_school_name,itcast_subject_id,itcast_subject_name),
              (year_month,group_type,origin_type,origin_channel),
              (year_month,group_type,origin_type,consulting_center),
               (dt,group_type),
              (dt,group_type,origin_type),
              (dt,group_type,origin_type,itcast_school_id,itcast_school_name),
              (dt,group_type,origin_type,itcast_subject_id,itcast_subject_name),
              (dt,group_type,origin_type,itcast_school_id,itcast_school_name,itcast_subject_id,itcast_subject_name),
              (dt,group_type,origin_type,origin_channel),
              (dt,group_type,origin_type,consulting_center)
))
select
    time_type,
    year_code,
    year_month,
    substring (year_month,6,2) as month_code,
  --  group_type_old,
    group_type_new,
    origin_type,
    itcast_school_id,
    itcast_school_name,
    itcast_subject_id,
    itcast_subject_name,
    origin_channel,
    consulting_center,
    application_cnt,
    dt
from tmp where group_type_old = group_type_new;