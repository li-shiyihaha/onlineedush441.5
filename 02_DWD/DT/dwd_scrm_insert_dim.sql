------------------因为采用了动态分区插入技术 因此需要设置相关参数---------------
--分区
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;
--hive压缩
set hive.exec.compress.intermediate=true;
set hive.exec.compress.output=true;
--写入时压缩生效
set hive.exec.orc.compression.strategy=COMPRESSION;

--客户静态信息表
insert overwrite table oe_dwd.dim_customer partition (dt)
select id,
       customer_relationship_id,
       create_date_time,
       update_date_time,
       deleted,
       name,
       idcard,
       birth_year,
       gender,
       phone,
       wechat,
       qq,
       email,
       split(area, ' ') as area_array,
       leave_school_date,
       graduation_date,
       bxg_student_id,
       creator,
       origin_type,
       origin_channel,
       md_id,
       dt
from oe_ods.t_customer
where deleted = 'false';

--线索申诉信息维度表
insert overwrite table oe_dwd.dim_customer_appeal partition (dt)
select id,
       customer_relationship_first_id,
       employee_id,
       employee_name,
       employee_department_id,
       employee_tdepart_id,
       appeal_status,
       create_date_time,
       update_date_time,
       deleted,
       dt
from oe_ods.t_customer_appeal
where customer_relationship_first_id is not null
  and customer_relationship_first_id != ''
  and deleted = 'false';

--员工信息维度表
insert overwrite table oe_dwd.dim_employee partition (dt)
select id,
       real_name,
       phone,
       department_id,
       department_name,
       job_number,
       last_login_date,
       creator,
       create_date_time,
       update_date_time,
       deleted,
       scrm_department_id,
       leave_office,
       leave_office_time,
       reinstated_time,
       superior_leaders_id,
       tdepart_id,
       dt
from oe_ods.t_employee
where tdepart_id is not null
  and tdepart_id != ''
  and deleted = 'false';

--员工部门维度表
insert overwrite table oe_dwd.dim_scrm_department partition (dt)
select id,
       name,
       parent_id,
       create_date_time,
       update_date_time,
       deleted,
       tdepart_code,
       creator,
       depart_level,
       depart_sign,
       dt
from oe_ods.t_scrm_department
where name is not null
  and name != ''
  and deleted = 'false';

--学科信息维度表
insert overwrite table oe_dwd.dim_itcast_subject partition (dt)
select id,
       create_date_time,
       update_date_time,
       deleted,
       name,
       code,
       dt
from oe_ods.t_itcast_subject
where name is not null
  and name != ''
  and deleted = 'false';

--校区信息维度表
insert overwrite table oe_dwd.dim_itcast_school partition (dt)
select id,
       create_date_time,
       update_date_time,
       deleted,
       name,
       code,
       dt
from oe_ods.t_itcast_school
where name is not null
  and name != ''
  and deleted = 'false';