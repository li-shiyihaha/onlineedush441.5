-- 赵长浩
-- dim_employee  建表语句
create table if not exists oe_dwd.dim_employee (
    id                  string ,
    email               string            comment '公司邮箱，OA登录账号',
    real_name           string            comment '员工的真实姓名',
    phone               string            comment '手机号，目前还没有使用；隐私问题OA接口没有提供这个属性，',
    department_id       string            comment 'OA中的部门编号，有负值',
    department_name     string            comment 'OA中的部门名',
    remote_login        string            comment '员工是否可以远程登录',
    job_number          string            comment '员工工号',
    cross_school        string            comment '是否有跨校区权限',
    last_login_date     string            comment '最后登录日期',
    creator             string            comment '创建人',
    create_date_time    string            comment '创建时间',
    update_date_time    string            comment '最后更新时间',
    deleted             string            comment '是否被删除（禁用）',
    scrm_department_id  string            comment 'SCRM内部部门id',
    leave_office        string            comment '离职状态',
    leave_office_time   string            comment '离职时间',
    reinstated_time     string            comment '复职时间',
    superior_leaders_id string            comment '上级领导ID',
    tdepart_id          string            comment '直属部门',
    tenant              string                  ,
    ems_user_name       string
)partitioned by (dt string)
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');
-- dim_itcast_clazz  建表语句
create table if not exists oe_dwd.dim_itcast_clazz (
    id                  string                comment 'ems课程id(非自增)',
    create_date_time    string                comment '创建时间',
    update_date_time    string                comment '最后更新时间',
    deleted             string                comment '是否被删除（禁用）',
    itcast_school_id    string                comment 'ems校区ID',
    itcast_school_name  string                comment 'ems校区名称',
    itcast_subject_id   string                comment 'ems学科ID',
    itcast_subject_name string                comment 'ems学科名称',
    itcast_brand        string                comment 'ems品牌',
    clazz_type_state    string                comment '班级类型状态',
    clazz_type_name     string                comment '班级类型名称',
    teaching_mode       string                comment '授课模式',
    start_time          string                comment '开班时间',
    end_time            string                comment '毕业时间',
    comment             string                comment '备注',
    detail              string                comment '详情(比如：27期)',
    uncertain           tinyint               comment '待定班(0:否,1:是)',
    tenant              string
)partitioned by (dt string)
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');
-- dim_scrm_department  建表语句
create table if not exists oe_dwd.dim_scrm_department (
    id               string    comment '部门id',
    name             string    comment '部门名称',
    parent_id        string    comment '父部门id',
    create_date_time string    comment '创建时间',
    update_date_time string    comment '更新时间',
    deleted          string    comment '删除标志',
    id_path          string    comment '编码全路径',
    tdepart_code     string    comment '直属部门',
    creator          string    comment '创建者',
    depart_level     string    comment '部门层级',
    depart_sign      string    comment '部门标志，暂时默认1',
    depart_line      string    comment '业务线，存储业务线编码',
    depart_sort      string    comment '排序字段',
    disable_flag     string    comment '禁用标志',
    tenant           string
)partitioned by (dt string)
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');
-- dim_customer_appeal  建表语句
create table if not exists oe_dwd.dim_customer_appeal (
    id                             string   ,
    customer_relationship_first_id string     comment '第一条客户关系id',
    employee_id                    string     comment '申诉人',
    employee_name                  string     comment '申诉人姓名',
    employee_department_id         string     comment '申诉人部门',
    employee_tdepart_id            string     comment '申诉人所属部门',
    appeal_status                  tinyint    comment '申诉状态，0:待稽核 1:无效 2：有效',
    audit_id                       string     comment '稽核人id',
    audit_name                     string     comment '稽核人姓名',
    audit_department_id            string     comment '稽核人所在部门',
    audit_department_name          string     comment '稽核人部门名称',
    audit_date_time                string     comment '稽核时间',
    create_date_time               string     comment '创建时间（申诉时间）',
    update_date_time               string     comment '更新时间',
    deleted                        string     comment '删除标志位',
    tenant                         string
)partitioned by (dt string)
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');