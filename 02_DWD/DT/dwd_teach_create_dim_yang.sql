
-- step0:ods建库
create database if not exists oe_dwd;
use oe_dwd;

--insert 分区
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;
--hive压缩
set hive.exec.compress.intermediate=true;
set hive.exec.compress.output=true;
--写入时压缩生效


-- step1.1: dwd 建表-在读学员人数信息表
DROP TABLE if exists oe_dwd.dim_stu_cnt;
create table if not exists oe_dwd.dim_stu_cnt
(
    `id`                     string ,
    `school_id`              string comment '校区id',
    `class_id`               string comment '班级id',
    `stu_cnt`                string comment '班级人数',
    `studying_date`          string comment '在读日期'
)comment '在读学员人数信息表'
partitioned by (dt string)
row format delimited
fields terminated by '\t' --指定分隔符制表符
stored as orc tblproperties ('orc.compress'='snappy');

/*
INSERT overwrite TABLE oe_dwd.dim_stu_cnt PARTITION(dt)
select id, school_id, class_id, studying_student_count, studying_date, dt
from oe_ods.t_class_studying_student_count;
*/



-- step3.1: 班级排课信息表 t_course_table_upload_detail
DROP TABLE if exists oe_dwd.dim_upload;
CREATE TABLE if not exists oe_dwd.dim_upload
(
    `id`                  string ,
    `base_id`             string  comment '课程主表id',
    `class_id`            string  comment '班级id',
    `class_date`          string  comment '上课日期',
    `content`             string  comment '课程内容',
    `class_mode`          int     comment '上课模式 0 传统全天 1 AB上午 2 AB下午 3 线上直播',
    `create_time`         string  comment '创建时间'
)
comment '班级排课信息表'
partitioned by (dt string)
row format delimited fields terminated by '\t' stored as orc tblproperties ('orc.compress'='snappy');

/*
INSERT overwrite TABLE oe_dwd.dim_upload PARTITION(dt)
select id,
       base_id,
       class_id,
       class_date,
       content,
       class_mode,
       create_time,dt
from oe_ods.t_course_table_upload_detail;
  */

-- step4.1: ods建表-班级作息时间表
DROP TABLE if exists oe_dwd.dim_class;
create table if not exists oe_dwd.dim_class
(
    `id`                    string,
    `class_id`              string comment '班级id',
    `morning_template_id`   string comment '上午出勤模板id',
    `morning_begin_time`    string comment '上午开始时间',
    `morning_end_time`      string comment '上午结束时间',
    `afternoon_template_id` string comment '下午出勤模板id',
    `afternoon_begin_time`  string comment '下午开始时间',
    `afternoon_end_time`    string comment '下午结束时间',
    `evening_template_id`   string comment '晚上出勤模板id',
    `evening_begin_time`    string comment '晚上开始时间',
    `evening_end_time`      string comment '晚上结束时间',
    `use_begin_date`        string comment '使用开始日期',
    `use_end_date`          string comment '使用结束日期',
    `create_time`           string comment '创建时间'
)comment '班级作息时间表'
partitioned by (dt string)
row format delimited
fields terminated by '\t' --指定分隔符制表符
stored as orc tblproperties ('orc.compress'='snappy');


/*
INSERT overwrite TABLE oe_dwd.dim_class PARTITION(dt)
select id,
       class_id,
       morning_template_id,
       morning_begin_time,
       morning_end_time,
       afternoon_template_id,
       afternoon_begin_time,
       afternoon_end_time,
       evening_template_id,
       evening_begin_time,
       evening_end_time,
       use_begin_date,
       use_end_date,
       create_time,
       dt
from oe_ods.t_tbh_class_time_table;
*/


-- step6.1: ods建表-学日历表calendar
DROP TABLE if exists oe_dwd.dim_cal;
create table if not exists oe_dwd.dim_cal
(
    `id`       string ,
    `datelist` string
)comment '日历表'
row format delimited
fields terminated by '\t' --指定分隔符制表符
stored as orc tblproperties ('orc.compress'='snappy');

/*
-- step6.2: insert日历表
INSERT overwrite TABLE oe_dwd.dim_cal
select * from oe_ods.t_calendar;
*/






