create database if not exists oe_dwd;
use oe_dwd;

--客户静态信息维度表
drop table if exists oe_dwd.dim_customer;
create table oe_dwd.dim_customer
(
    id                       string comment '用户id',
    customer_relationship_id string comment '当前意向id',
    create_date_time         string comment '创建时间',
    update_date_time         string comment '最后更新时间',
    deleted                  string comment '是否被删除（禁用）',
    name                     string comment '姓名',
    idcard                   string comment '身份证号',
    birth_year               string comment '出生年份',
    gender                   string comment '性别',
    phone                    string comment '手机号',
    wechat                   string comment '微信',
    qq                       string comment 'qq号',
    email                    string comment '邮箱',
    area_array               array<string> comment '所在区域',
    leave_school_date        string comment '离校时间',
    graduation_date          string comment '毕业时间',
    bxg_student_id           string comment '博学谷学员ID，可能未关联到，不存在',
    creator                  string comment '创建人ID',
    origin_type              string comment '数据来源',
    origin_channel           string comment '来源渠道',
    md_id                    string comment '中台id'
) comment '客户静态信息表'
partitioned by (dt string)
row format delimited fields terminated by '\t'
stored as orc tblproperties ('orc.compress' = 'SNAPPY');

--线索申诉信息维度表
drop table if exists oe_dwd.dim_customer_appeal;
create table oe_dwd.dim_customer_appeal
(
    id                             string comment '序号主键',
    customer_relationship_first_id string comment '第一条客户关系id',
    employee_id                    string comment '申诉人',
    employee_name                  string comment '申诉人姓名',
    employee_department_id         string comment '申诉人部门',
    employee_tdepart_id            string comment '申诉人所属部门',
    appeal_status                  tinyint comment '申诉状态，0:待稽核 1:无效 2：有效',
    create_date_time               string comment '创建时间（申诉时间）',
    update_date_time               string comment '更新时间',
    deleted                        string comment '删除标志位'
) comment '线索申诉信息维度表'
partitioned by (dt string)
row format delimited fields terminated by '\t'
stored as orc tblproperties ('orc.compress' = 'SNAPPY');

--员工信息维度表
drop table if exists oe_dwd.dim_employee;
create table oe_dwd.dim_employee
(
    id                  string comment '员工id',
    real_name           string comment '员工的真实姓名',
    phone               string comment '手机号，目前还没有使用；隐私问题OA接口没有提供这个属性，',
    department_id       string comment 'OA中的部门编号，有负值',
    department_name     string comment 'OA中的部门名',
    job_number          string comment '员工工号',
    last_login_date     string comment '最后登录日期',
    creator             string comment '创建人',
    create_date_time    string comment '创建时间',
    update_date_time    string comment '最后更新时间',
    deleted             string comment '是否被删除（禁用）',
    scrm_department_id  string comment 'SCRM内部部门id',
    leave_office        string comment '离职状态',
    leave_office_time   string comment '离职时间',
    reinstated_time     string comment '复职时间',
    superior_leaders_id string comment '上级领导ID',
    tdepart_id          string comment '直属部门'
) comment '员工信息维度表'
partitioned by (dt string)
row format delimited fields terminated by '\t'
stored as orc tblproperties ('orc.compress' = 'SNAPPY');

--员工部门维度表
drop table if exists oe_dwd.dim_scrm_department;
create table oe_dwd.dim_scrm_department
(
    id               string comment '部门id',
    name             string comment '部门名称',
    parent_id        string comment '父部门id',
    create_date_time string comment '创建时间',
    update_date_time string comment '更新时间',
    deleted          string comment '删除标志',
    tdepart_code     string comment '直属部门',
    creator          string comment '创建者',
    depart_level     string comment '部门层级',
    depart_sign      string comment '部门标志，暂时默认1'
) comment '员工部门维度表'
partitioned by (dt string)
row format delimited fields terminated by '\t'
stored as orc tblproperties ('orc.compress' = 'SNAPPY');

--学科信息维度表
drop table if exists oe_dwd.dim_itcast_subject;
create table oe_dwd.dim_itcast_subject
(
    id               string comment '学科id',
    create_date_time string comment '创建时间',
    update_date_time string comment '最后更新时间',
    deleted          string comment '是否被删除（禁用）',
    name             string comment '学科名称',
    code             string comment '学科code名'
) comment '学科信息维度表'
partitioned by (dt string)
row format delimited fields terminated by '\t'
stored as orc tblproperties ('orc.compress' = 'SNAPPY');

--校区信息维度表
drop table if exists oe_dwd.dim_itcast_school;
create table oe_dwd.dim_itcast_school
(
    id               string comment '校区id',
    create_date_time string comment '创建时间',
    update_date_time string comment '最后更新时间',
    deleted          string comment '是否被删除（禁用）',
    name             string comment '校区名称',
    code             string comment '校区code名'
) comment '校区信息维度表'
partitioned by (dt string)
row format delimited fields terminated by '\t'
stored as orc tblproperties ('orc.compress' = 'SNAPPY');