drop table if exists dim_stu_cnt;
create table if not exists oe_dwd.dim_stu_cnt(
    id  string,
    school_id string comment '校区id',
    class_id  string comment '班级id',
    stu_cnt  string comment '班级人数',
    studying_date string
)comment '班级在读人数总信息表'
partitioned by (dt string)
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compress'='snappy');

drop table if exists oe_dwd.dim_cal;
create table if not exists oe_dwd.dim_cal(
    id string,
    datelist string
) comment '日历表'
row format delimited
fields terminated by '\t'
tblproperties ('orc.compress'='snappy')
;

drop table  if exists oe_dwd.dim_upload;
create table if not exists oe_dwd.dim_upload(
    id string,
    base_id string comment '课程主表id',
    class_id  string comment '班级id',
    class_date string comment '上课日期',
    content string comment '课程内容  为null时即为休息或开班典礼等 无效打卡或无效请假',
    class_mode string comment '上课模式 0 传统全天 1 上午 2 下午 3 线上直播',
    create_time string comment '创建时间'
)comment '班级排课信息表'
partitioned by (dt string)
row format delimited
fields terminated by '\t'
tblproperties ('orc.compress'='snappy');

drop table if exists oe_dwd.dim_class;
create table if not exists oe_dwd.dim_class(
        id string,
        class_id string comment '班级id',
        morning_template_id string comment '上午出勤模板id',
        morning_begin_time string comment '上午开始时间',
        morning_end_time string comment '上午结束时间',
        afternoon_template_id string comment '下午出勤模板id',
        afternoon_begin_time string comment '下午开始时间',
        afternoon_end_time string comment '下午结束时间',
        evening_template_id string comment '晚上出勤模板id',
        evening_begin_time string comment '晚上开始时间',
        evening_end_time string comment '晚上结束时间',
        use_begin_date string comment '使用开始日期',
        use_end_date string comment '使用开始日期',
        create_time string comment '创建时间'
)comment '班级作息时间表'
partitioned by (dt string)
row format delimited
fields terminated by '\t'
tblproperties ('orc.compress'='snappy')
;