--dwd 访问主题建表
------------------因为采用了动态分区插入技术 因此需要设置相关参数---------------
--分区
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;
--hive压缩
set hive.exec.compress.intermediate=true;
set hive.exec.compress.output=true;
--写入时压缩生效
set hive.exec.orc.compression.strategy=COMPRESSION;


-- 访问咨询dim表
insert into table oe_dwd.dwd_dim_web_chat_text_ems_2019_07 partition (start_date)
select
     id
    ,from_url
    ,dt  as start_date
from oe_ods.t_web_chat_text_ems_2019_07;

