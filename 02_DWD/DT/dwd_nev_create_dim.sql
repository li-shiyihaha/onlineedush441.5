--dwd 访问主题建表
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;
--hive压缩
set hive.exec.compress.intermediate=true;
set hive.exec.compress.output=true;
create database if not exists oe_dwd;
use oe_dwd;
---访问咨询信息dim表
drop table if exists oe_dwd.dwd_web_chat_ems_2019_07;
drop table if exists oe_dwd.dwd_dim_web_chat_text_ems_2019_07;
create table         oe_dwd.dwd_dim_web_chat_text_ems_2019_07
(
    id                   string   comment '主键'
    ,from_url             string   comment '会话来源页面'
-- end_date string  comment '拉链结束日期'
)
comment '访问咨询信息副表'
    partitioned by (start_date string)   -- 需要改一下分区 字段
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'ZLIB');
