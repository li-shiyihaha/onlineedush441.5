-- 赵长浩
-- dim_customer_appeal  数据清洗同步
insert overwrite table oe_dwd.dim_customer_appeal partition (dt)
select
    id,
    customer_relationship_first_id,
    employee_id,
    employee_name,
    employee_department_id,
    employee_tdepart_id,
    case appeal_status when 0 then '待稽核'
                       when 1 then '无效'
                       when 2 then '有效'
                       else 'other' end as appeal_status,
    audit_id,
    audit_name,
    audit_department_id,
    audit_department_name,
    audit_date_time,
    create_date_time,
    update_date_time,
    deleted,
    tenant,
    dt
from oe_ods.t_customer_appeal;
-- dim_employee  数据清洗同步
insert overwrite table oe_dwd.dim_employee partition (dt)
select
    id,
    email,
    real_name,
    phone,
    department_id,
    department_name,
    remote_login,
    job_number,
    cross_school,
    last_login_date,
    creator,
    create_date_time,
    update_date_time,
    deleted,
    scrm_department_id,
    leave_office,
    leave_office_time,
    reinstated_time,
    superior_leaders_id,
    tdepart_id,
    tenant,
    ems_user_name,
    dt
from oe_ods.t_employee;
-- dim_itcast_clazz  数据清洗同步
insert overwrite table oe_dwd.dim_itcast_clazz partition (dt)
select
    id,
    create_date_time,
    update_date_time,
    deleted,
    itcast_school_id,
    itcast_school_name,
    itcast_subject_id,
    itcast_subject_name,
    itcast_brand,
    clazz_type_state,
    clazz_type_name,
    teaching_mode,
    start_time,
    end_time,
    comment,
    detail,
    uncertain,
    tenant,
    dt
from oe_ods.t_itcast_clazz;
-- dim_scrm_department  数据清洗同步
insert overwrite table oe_dwd.dim_scrm_department partition (dt)
select
    id,
    name,
    parent_id,
    create_date_time,
    update_date_time,
    deleted,
    id_path,
    tdepart_code,
    creator,
    depart_level,
    depart_sign,
    depart_line,
    depart_sort,
    disable_flag,
    tenant,
    dt
from oe_ods.t_scrm_department;