drop table if exists oe_dwd.fac_signin;
create table if not exists oe_dwd.fac_signin(
    id string,
    normal_class_flag  int comment '是否正课 1 正课 2 自习',
    time_table_id      string comment '作息时间id 关联维度表',
    class_id           string  comment '班级id',
    student_id         string  comment '学员id',
    signin_time       string comment '签到时间',
    signin_date       string comment '签到日期',
    signin_type       STRING   comment '签到类型 1  正常打卡 2 老师补卡',
    share_state       string  comment '共享屏幕状态 0 否 1 是 在上午或者下午段有共屏记录，则该段所有记录该字段为1'
)comment '签到事实表'
partitioned by (dt string)
row format delimited
fields terminated by '\t'
stored as orc tblproperties ('orc.compress'='snappy');
-- 创建事实表 fac_leave_apply
drop  table if exists oe_dwd.fac_leave;
create table if not exists oe_dwd.fac_leave(
    id string,
    class_id           string  comment '班级id',
    student_id         string  comment '学员id',
    audit_state         string comment '审核状态 0 待审核 1 通过 2 不通过' ,
    leave_type       int comment '请假类型 1 事假 2 销假',
    begin_time        string comment '请假开始时间',
    begin_time_type    int   comment '1 上午 2 ：下午',
    end_time       string  comment '请假结束时间',
    end_time_type          int comment '1：上午 2 下午',
    days           string comment '请假/已休天数',
    cancel_state  tinyint comment '撤销状态 0 未撤销 1 已撤销',
    cancel_time  string comment '撤销时间',
    `old_leave_id`  string comment '原请假id，只有leave_type =2 销假的时候才有',
    valid_state   tinyint comment '是否有效（0：无效 1 ：有效）',
    create_time   string comment '创建时间'
)comment '请假事实表'
partitioned by (dt string)
row format delimited
fields terminated by '\t'
stored as orc tblproperties ('orc.compress'='snappy');