-- 赵长浩
-- 因为采用了动态分区插入技术 因此需要设置相关参数
-- 分区
/*SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;
--hive压缩
set hive.exec.compress.intermediate=true;
set hive.exec.compress.output=true;
--写入时压缩生效
set hive.exec.orc.compression.strategy=COMPRESSION;*/

-- fact_customer_relationship 数据清洗同步(拉链表)
insert overwrite table oe_dwd.fact_customer_relationship partition (start_date)
select
    id,
    create_date_time,
    update_date_time,
    deleted,
    customer_id,
    first_id,
    belonger,
    belonger_name,
    initial_belonger,
    distribution_handler,
    business_scrm_department_id,
    last_visit_time,
    next_visit_time,
    case origin_type when 'NETSERVICE'  then '线上'
                     when  'OTHER'  then  '线下'
                      else 'other'  end  as origin_type,
    itcast_school_id,
    itcast_subject_id,
    intention_study_type,
    anticipat_signup_date,
    level,
    creator,
    current_creator,
    creator_name,
    origin_channel,
    first_customer_clue_id,
    last_customer_clue_id,
    process_state,
    process_time,
    payment_state,
    payment_time,
    signup_state,
    signup_time,
    notice_state,
    notice_time,
    lock_state,
    lock_time,
    itcast_clazz_id,
    itcast_clazz_time,
    payment_url,
    payment_url_time,
    ems_student_id,
    delete_reason,
    deleter,
    deleter_name,
    delete_time,
    course_id,
    course_name,
    delete_comment,
    close_state,
    close_time,
    appeal_id,
    tenant,
    total_fee,
    belonged,
    belonged_time,
    transfer,
    transfer_time,
    follow_type,
    transfer_bxg_oa_account,
    transfer_bxg_belonger_name,
    '9999-99-99' as end_date,
    dt as start_date
from oe_ods.t_customer_relationship;
-- MySQL数据源 插入一条数据，更新一条数据
/*insert into scrm.customer_relationship values
(999999,'2024-01-02 09:07:41','2024-01-02 20:47:31',false,100000,100000,null,null,null,null,43,'2024-01-02 00:00:00',null,'SCHOOL',1,null,null,null,'C',1578,1578,'刘晓曲',"",'毕业学校：安徽科技学院，计划报读班级：null',100000,100000,null,null,null,null,null,null,null,null,false,null,null,null,null,null,null,null,1578,'刘晓曲','2024-01-02 03:12:32',null,null,null,'RELEASE','2024-01-02 03:12:32',null,1,null,1578,null,null,null,null,1,null,null
);
update scrm.customer_relationship set create_date_time='2024-01-02 09:17:41'
where id=100000;
update scrm.customer_relationship set update_date_time='2024-01-03 09:17:41'
where id=100000;
update scrm.customer_relationship set last_visit_time='2024-02-02 09:17:41'
where id=100000;
update scrm.customer_relationship set delete_time='2024-03-02 09:17:41'
where id=100000;
update scrm.customer_relationship set close_time='2024-03-02 09:27:41'
where id=100000;
-- 查询验证MySQL数据源中的数据
select * from scrm.customer_relationship;*/
-- 增量数据采集
/*/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/scrm?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select *, '2024-01-02' as dt from customer_relationship where id is not null and 1=1 and (create_date_time between '2024-01-02 00:00:00' and '2024-01-02 23:59:59') or (update_date_time between '2024-01-02 00:00:00' and '2024-01-02 23:59:59') and \$CONDITIONS" \
--input-null-string '\\N' --input-null-non-string '\\N' \
--hcatalog-database oe_ods \
--hcatalog-table t_customer_relationship \
-m 1*/
-- 拉链表left join 增量表  as 结果集A
insert overwrite table oe_dwd.fact_customer_relationship_tmp partition (start_date)
select
    fcr.id,
    fcr.create_date_time,
    fcr.update_date_time,
    cast(fcr.deleted as string) as deleted,
    fcr.customer_id,
    fcr.first_id,
    fcr.belonger,
    fcr.belonger_name,
    fcr.initial_belonger,
    fcr.distribution_handler,
    fcr.business_scrm_department_id,
    fcr.last_visit_time,
    fcr.next_visit_time,
    fcr.origin_type,
    fcr.itcast_school_id,
    fcr.itcast_subject_id,
    fcr.intention_study_type,
    fcr.anticipat_signup_date,
    fcr.level,
    fcr.creator,
    fcr.current_creator,
    fcr.creator_name,
    fcr.origin_channel,
    fcr.first_customer_clue_id,
    fcr.last_customer_clue_id,
    fcr.process_state,
    fcr.process_time,
    fcr.payment_state,
    fcr.payment_time,
    fcr.signup_state,
    fcr.signup_time,
    fcr.notice_state,
    fcr.notice_time,
    fcr.lock_state,
    fcr.lock_time,
    fcr.itcast_clazz_id,
    fcr.itcast_clazz_time,
    fcr.payment_url,
    fcr.payment_url_time,
    fcr.ems_student_id,
    fcr.delete_reason,
    fcr.deleter,
    fcr.deleter_name,
    fcr.delete_time,
    fcr.course_id,
    fcr.course_name,
    fcr.delete_comment,
    fcr.close_state,
    fcr.close_time,
    fcr.appeal_id,
    fcr.tenant,
    fcr.total_fee,
    fcr.belonged,
    fcr.belonged_time,
    fcr.transfer,
    fcr.transfer_time,
    fcr.follow_type,
    fcr.transfer_bxg_oa_account,
    fcr.transfer_bxg_belonger_name,
    if(tcr.id is not null and fcr.end_date = '9999-99-99',date_add(tcr.dt,-1),fcr.end_date) as end_date,
    fcr.start_date
from oe_dwd.fact_customer_relationship fcr
left join (select * from oe_ods.t_customer_relationship where dt = '2024-01-02') tcr on fcr.id=tcr.id
-- 结果集A union all 增量表
union all
select
id,
    create_date_time,
    update_date_time,
    deleted,
    customer_id,
    first_id,
    belonger,
    belonger_name,
    initial_belonger,
    distribution_handler,
    business_scrm_department_id,
    last_visit_time,
    next_visit_time,
    case origin_type when 'NETSERVICE'  then '线上'
                     when  'OTHER'  then  '线下'
                      else 'other'  end  as origin_type,
    itcast_school_id,
    itcast_subject_id,
    intention_study_type,
    anticipat_signup_date,
    level,
    creator,
    current_creator,
    creator_name,
    origin_channel,
    first_customer_clue_id,
    last_customer_clue_id,
    process_state,
    process_time,
    payment_state,
    payment_time,
    signup_state,
    signup_time,
    notice_state,
    notice_time,
    lock_state,
    lock_time,
    itcast_clazz_id,
    itcast_clazz_time,
    payment_url,
    payment_url_time,
    ems_student_id,
    delete_reason,
    deleter,
    deleter_name,
    delete_time,
    course_id,
    course_name,
    delete_comment,
    close_state,
    close_time,
    appeal_id,
    tenant,
    total_fee,
    belonged,
    belonged_time,
    transfer,
    transfer_time,
    follow_type,
    transfer_bxg_oa_account,
    transfer_bxg_belonger_name,
    '9999-99-99' as end_date,
    '2024-01-02' as start_date
from oe_ods.t_customer_relationship
where dt = '2024-01-02';

-- 结果插入临时表验证
create table if not exists oe_dwd.fact_customer_relationship_tmp (
    id     string,
    create_date_time              string         comment '创建时间',
    update_date_time              string         comment '最后更新时间',
    deleted                       tinyint        comment '是否被删除（禁用)',
    customer_id                   string         comment '所属客户id',
    first_id                      string         comment '第一条客户关系id',
    belonger                      string         comment '归属人',
    belonger_name                 string         comment  '归属人姓名',
    initial_belonger              string         comment  '初始归属人',
    distribution_handler          string         comment  '分配处理人',
    business_scrm_department_id   string         comment   '归属部门',
    last_visit_time               string         comment  '最后回访时间',
    next_visit_time               string         comment  '下次回访时间',
    origin_type                   string         comment  '数据来源',
    itcast_school_id              string         comment  '校区Id',
    itcast_subject_id             string         comment  '学科Id',
    intention_study_type          string         comment  '意向学习方式',
    anticipat_signup_date         string         comment   '预计报名时间',
    level                         string         comment   '客户级别',
    creator                       string         comment    '创建人',
    current_creator               string         comment   '当前创建人：初始==创建人，当在公海拉回时为 拉回人',
    creator_name                  string         comment   '创建者姓名',
    origin_channel                string         comment   '来源渠道',
    first_customer_clue_id        string         comment    '第一条线索id',
    last_customer_clue_id         string         comment    '最后一条线索id',
    process_state                 string         comment  '处理状态',
    process_time                  string         comment  '处理状态变动时间',
    payment_state                 string         comment  '支付状态',
    payment_time                  string         comment  '支付状态变动时间',
    signup_state                  string         comment  '报名状态',
    signup_time                   string         comment '报名时间',
    notice_state                  string         comment '通知状态',
    notice_time                   string         comment '通知状态变动时间',
    lock_state                    string         comment '锁定状态',
    lock_time                     string         comment '锁定状态修改时间',
    itcast_clazz_id               string         comment '所属ems班级id',
    itcast_clazz_time             string         comment '报班时间',
    payment_url                   string         comment '付款链接',
    payment_url_time              string         comment '支付链接生成时间',
    ems_student_id                string         comment 'ems的学生id',
    delete_reason                 string         comment '删除原因',
    deleter                       string         comment '删除人',
    deleter_name                  string         comment '删除人姓名',
    delete_time                   string         comment '删除时间',
    course_id                     string         comment '课程ID',
    course_name                   string         comment '课程名称',
    delete_comment                string         comment '删除原因说明',
    close_state                   string         comment '关闭装填',
    close_time                    string         comment '关闭状态变动时间',
    appeal_id                     string         comment '申诉id',
    tenant                        string         comment '租户',
    total_fee                     string         comment '报名费总金额',
    belonged                      string         comment '小周期归属人',
    belonged_time                 string         comment '归属时间',
    transfer                      string         comment '转移人',
    transfer_time                 string         comment '转移时间',
    follow_type                   string         comment '分配类型，0-自动分配，1-手动分配，2-自动转移，3-手动单个转移，4-手动批量转移，5-公海领取',
    transfer_bxg_oa_account       string         comment '转移到博学谷归属人OA账号',
    transfer_bxg_belonger_name    string         comment '转移到博学谷归属人OA姓名',
    end_date                      string         comment  '拉链结束日期'
)partitioned by (start_date string)               --拉链起始时间 也是表分区字段
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');
-- 验证无误覆盖原表
insert overwrite table oe_dwd.fact_customer_relationship partition (start_date)
select * from oe_dwd.fact_customer_relationship_tmp;

-- fact_customer_clue  数据清洗同步
insert overwrite table oe_dwd.fact_customer_clue partition (dt)
select
    id,
    create_date_time,
    update_date_time,
    deleted,
    customer_id,
    customer_relationship_id,
    session_id,
    sid,
    status,
    `user`,
    create_time,
    platform,
    s_name,
    seo_source,
    seo_keywords,
    ip,
    referrer,
    from_url,
    landing_page_url,
    url_title,
    to_peer,
    manual_time,
    begin_time,
    reply_msg_count,
    total_msg_count,
    msg_count,
    comment,
    finish_reason,
    finish_user,
    end_time,
    platform_description,
    browser_name,
    os_info,
    area,
    country,
    province,
    city,
    creator,
    name,
    idcard,
    phone,
    itcast_school_id,
    itcast_school,
    itcast_subject_id,
    itcast_subject,
    wechat,
    qq,
    email,
    gender,
    level,
    origin_type,
    information_way,
    working_years,
    technical_directions,
    customer_state,
    valid,
    anticipat_signup_date,
    clue_state,
    scrm_department_id,
    superior_url,
    superior_source,
    landing_url,
    landing_source,
    info_url,
    info_source,
    origin_channel,
    course_id,
    course_name,
    zhuge_session_id,
    is_repeat,
    tenant,
    activity_id,
    activity_name,
    follow_type,
    shunt_mode_id,
    shunt_employee_group_id,
    dt
from oe_ods.t_customer_clue;