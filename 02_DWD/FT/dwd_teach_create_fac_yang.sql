
-- step2.1: 建表 学生请假申请表
create database if not exists oe_dwd;
use oe_dwd;
DROP TABLE if exists oe_dwd.fac_leave;
create table if not exists oe_dwd.fac_leave
(
    `id`              string  ,
    `class_id`        string            comment '班级id',
    `student_id`      string            comment '学员id',
    `audit_state`     tinyint            comment '审核状态 0 待审核 1 通过 2 不通过',
    `leave_type`      int            comment '请假类型  1 请假 2 销假',
    `begin_time`      string            comment '请假开始时间',
    `begin_time_type` int            comment '1：上午 2：下午',
    `end_time`        string            comment '请假结束时间',
    `end_time_type`   int            comment '1：上午 2：下午',
    `days`            string            comment '请假/已休天数',
    `cancel_state`    tinyint            comment '撤销状态  0 未撤销 1 已撤销',
    `cancel_time`     string            comment '撤销时间',
    `valid_state`     tinyint            comment '是否有效（0：无效 1：有效）',
    `create_time`     string            comment '创建时间'
)comment '学生请假申请表' partitioned by(dt string) -- 天分区
row format delimited fields terminated by '\t'
stored as orc tblproperties ('orc.compress'='snappy');

/*
INSERT overwrite TABLE oe_dwd.fac_leave partition(dt)
select id,
       class_id,
       student_id,
       audit_state,
       leave_type,
       leave_reason,
       begin_time,
       begin_time_type,
       end_time,
       end_time_type,
       days,
       cancel_state,
       cancel_time,
       old_leave_id,
       valid_state,
       create_time,
       dt
from oe_ods.t_student_leave_apply;
*/


-- step5.1: ods建表-学生打卡记录表tbh_student_signin_record




