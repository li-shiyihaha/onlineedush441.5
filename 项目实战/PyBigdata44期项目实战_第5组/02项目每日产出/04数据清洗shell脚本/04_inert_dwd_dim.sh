#! /bin/bash
export LANG=zh_CN.UTF-8
HIVE_HOME=/usr/bin/hive


${HIVE_HOME} -S -e "

-- step1.2: 导入在读学员人数信息表 insert
INSERT overwrite TABLE oe_dwd.dim_stu_cnt PARTITION(dt)
select id, school_id, class_id, studying_student_count, studying_date, dt
from oe_ods.t_class_studying_student_count;

-- step3.2  班级排课信息表
INSERT overwrite TABLE oe_dwd.dim_upload PARTITION(dt)
select id,
       base_id,
       class_id,
       class_date,
       content,
       class_mode,
       create_time,dt
from oe_ods.t_course_table_upload_detail;

-- step4.2: insert班级作息时间表
INSERT overwrite TABLE oe_dwd.dim_class PARTITION(dt)
select id,
       class_id,
       morning_template_id,
       morning_begin_time,
       morning_end_time,
       afternoon_template_id,
       afternoon_begin_time,
       afternoon_end_time,
       evening_template_id,
       evening_begin_time,
       evening_end_time,
       use_begin_date,
       use_end_date,
       create_time,
       dt
from oe_ods.t_tbh_class_time_table;


-- step6.2: insert日历表
INSERT overwrite TABLE oe_dwd.dim_cal
select id, datelist
from oe_ods.t_calendar;
"