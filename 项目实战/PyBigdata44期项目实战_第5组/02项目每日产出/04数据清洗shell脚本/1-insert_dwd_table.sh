#! /bin/bash
export LANG=zh_CN.UTF-8
HIVE_HOME=/usr/bin/hive


${HIVE_HOME} -S -e "
--dwd 访问主题建表
------------------因为采用了动态分区插入技术 因此需要设置相关参数---------------
--分区
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;
--hive压缩
set hive.exec.compress.intermediate=true;
set hive.exec.compress.output=true;
--写入时压缩生效
set hive.exec.orc.compression.strategy=COMPRESSION;


-- 访问咨询dim表
insert into table oe_dwd.dwd_dim_web_chat_text_ems_2019_07 partition (start_date)
select
     id
    ,from_url
    ,dt  as start_date
from oe_ods.t_web_chat_text_ems_2019_07;


--dwd  访客咨询fact表  --拉链


-- create + insert  ods层的数据 （包括新增和更新的数据） 进入dwd层
insert into table oe_dwd.dwd_fact_web_chat_ems_2019_07  partition(start_date)
select
     id
    ,session_id
    ,sid
    ,create_time
    ,seo_source
    ,ip
    ,area
    ,country
    ,province
    ,city
    ,origin_channel
    ,msg_count
    ,browser_name
    , '9999-99-99' as end_date
    ,dt  as  start_date
from oe_ods.t_web_chat_ems_2019_07;

show partitions oe_dwd.dwd_fact_web_chat_ems_2019_07 ;


-- a、在mysql里面 新增和更新数据

--新增访客用户
INSERT INTO nev.web_chat_ems_2019_07 (
    id
    ,create_date_time
    ,session_id
    ,sid
    ,create_time
    ,seo_source
    ,seo_keywords

    ,ip
    ,area

    ,country
    ,province
    ,city

    ,origin_channel
    ,`user`
    ,manual_time
    ,begin_time
    ,end_time
    ,last_customer_msg_time_stamp
    ,last_agent_msg_time_stamp
    ,reply_msg_count
    ,msg_count
    ,browser_name
    ,os_info
    )
     VALUES ('211198', '2024-01-02 02:57:00.0', 'c23423410-9b5a-11e9-ad03-638d7474afed', 'ec9923c0-9c05-11e9-844f-1508c4b66f99',
             '2019-07-01 21:42:00.0',
             '站内','' ,
            '106.75.33.59', '中国 上海', '中国' , '上海', '未知',
            '','2019-07-01 09:38:00.0', '2019-07-01 21:41:00' , null, null, null ,null, null, null, 'Firefox',
             'iOS 12.0');
select * from nev.web_chat_ems_2019_07 WHERE id='211198';

--更新id
UPDATE nev.web_chat_ems_2019_07 SET browser_name = 'chrome'
WHERE id='10008';
UPDATE nev.web_chat_ems_2019_07 SET create_time='2019-07-03 21:41:00'
WHERE id='10008';
delete from nev.web_chat_ems_2019_07 where create_time='2019-07-03 21:41:00' and id = '10007';



-- b、使用sqoop进行增量采集 把新增及更新的数据导入到ods新的分区中2021-11-30  【增量表】
/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/nev?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select *, '2024-01-02' as dt from web_chat_ems_2019_07 where 1=1 \
--      and create_date_time between '2024-01-02 00:00:00' and '2024-01-02 23:59:59' \
--      or  create_time between '2019-07-03 00:00:00' and '2019-07-03 23:59:59' and  \$CONDITIONS" \
--hcatalog-database oe_ods \
--hcatalog-table t_web_chat_ems_2019_07 \
-m 1
;



--c 拉链   表a left join 增量表b  ----》结果集A
--增量表
(select   id
    ,session_id
    ,sid
    ,create_time
    ,seo_source
    ,ip
    ,area
    ,country
    ,province
    ,city
    ,origin_channel
    ,msg_count
    ,browser_name
    ,   '9999-99-99' as end_date
    , dt as start_date
from oe_ods.t_web_chat_ems_2019_07
where dt = '2024-01-02' ;) b
--一条新增数据 10008
--一条更新数据 211198

select
     a.id
    ,a.session_id
    ,a.sid
    ,a.create_time
    ,a.seo_source
    ,a.ip
    ,a.area
    ,a.country
    ,a.province
    ,a.city
    ,a.origin_channel
    ,a.msg_count
    ,a.browser_name
    ,if (b.id is not null and a.end_date = '9999-99-99',date_sub(b.start_date,1),a.end_date)
    ,a. start_date
from oe_dwd.dwd_fact_web_chat_ems_2019_07 a
left join
    (select   id
             ,session_id
             ,sid
             ,create_time
             ,seo_source
             ,ip
             ,area
             ,country
             ,province
             ,city
             ,origin_channel
             ,msg_count
             ,browser_name
             ,   '9999-99-99' as end_date
             , dt as start_date
    from oe_ods.t_web_chat_ems_2019_07
    where dt = '2024-01-02') b
on a.id =b.id ;

---d.  结果集A union all 增量表
select
     a.id
    ,a.session_id
    ,a.sid
    ,a.create_time
    ,a.seo_source
    ,a.ip
    ,a.area
    ,a.country
    ,a.province
    ,a.city
    ,a.origin_channel
    ,a.msg_count
    ,a.browser_name
    ,if (b.id is not null and a.end_date = '9999-99-99',date_sub(b.start_date,1),a.end_date) as end_date
    ,a.start_date
from oe_dwd. dwd_fact_web_chat_ems_2019_07 a
left join
        (select  id
    ,session_id
    ,sid
    ,create_time
    ,seo_source
    ,ip
    ,area
    ,country
    ,province
    ,city
    ,origin_channel
    ,msg_count
    ,browser_name
    , '9999-99-99' as end_date
    , dt as start_date
from oe_ods.t_web_chat_ems_2019_07
where dt = '2024-01-02') b
on a.id =b.id
union all
select
    id
    ,session_id
    ,sid
    ,create_time
    ,seo_source
    ,ip
    ,area
    ,country
    ,province
    ,city
    ,origin_channel
    ,msg_count
    ,browser_name
    , '9999-99-99' as end_date
    , dt as start_date
from oe_ods.t_web_chat_ems_2019_07
where dt = '2024-01-02';
 --发现两条 脏数据 -- 在tmp 洗掉（where end_date= '2024-01-01' and start_date  = '2024-01-02'）

--e. 创建临时表 dwd fact tmp
drop table if exists oe_dwd.dwd_fact_web_chat_ems_2019_07_tmp;
create table  oe_dwd.dwd_fact_web_chat_ems_2019_07_tmp
(
    id                           string     comment '主键'        ,
    session_id                   string     comment '会话系统sessionId',
    sid                          string     comment '访客id',
    create_time                  string     comment '会话创建时间',
    seo_source                   string     comment '搜索来源',
    ip                           string     comment 'IP地址',
    area                         string     comment '地域',
    country                      string     comment '所在国家',
    province                     string     comment '省',
    city                         string     comment '城市',
    origin_channel               string     comment '来源渠道(广告)',
    msg_count                    int        comment '客户发送消息数',
    browser_name                 string     comment '浏览网页名字',
    end_date                     string     comment '拉链结束时间'
    )
comment '访问咨询信息主表'
    partitioned by (start_date string)
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'ZLIB');


insert overwrite table oe_dwd.dwd_fact_web_chat_ems_2019_07_tmp partition (start_date)
select
     a.id
    ,a.session_id
    ,a.sid
    ,a.create_time
    ,a.seo_source
    ,a.ip
    ,a.area
    ,a.country
    ,a.province
    ,a.city
    ,a.origin_channel
    ,a.msg_count
    ,a.browser_name
    ,if (b.id is not null and a.end_date = '9999-99-99',date_sub(b.start_date,1),a.end_date) as end_date
    ,a.start_date
from oe_dwd. dwd_fact_web_chat_ems_2019_07 a
left join

    (select  id
    ,session_id
    ,sid
    ,create_time
    ,seo_source
    ,ip
    ,area
    ,country
    ,province
    ,city
    ,origin_channel
    ,msg_count
    ,browser_name
    ,   '9999-99-99' as end_date
    , dt as start_date
from oe_ods.t_web_chat_ems_2019_07
where dt = '2024-01-02') b
on a.id =b.id
union all
select
    id
    ,session_id
    ,sid
    ,create_time
    ,seo_source
    ,ip
    ,area
    ,country
    ,province
    ,city
    ,origin_channel
    ,msg_count
    ,browser_name
    , '9999-99-99' as end_date
    , dt as start_date
from oe_ods.t_web_chat_ems_2019_07
where dt = '2024-01-02';

--f.校验
select * from oe_dwd.dwd_fact_web_chat_ems_2019_07_tmp where start_date = '2024-01-02';

--g。结果正确以后写入fact表 ，并同时筛选出最新更新数据，位置后数据计算做准备
insert overwrite table oe_dwd.dwd_fact_web_chat_ems_2019_07 partition(start_date)
select * from oe_dwd.dwd_fact_web_chat_ems_2019_07_tmp
where end_date = '9999-99-99' ;
"