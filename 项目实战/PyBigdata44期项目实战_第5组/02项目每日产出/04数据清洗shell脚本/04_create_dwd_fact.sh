
#! /bin/bash
export LANG=zh_CN.UTF-8
HIVE_HOME=/usr/bin/hive

${HIVE_HOME} -S -e "

-- step2.1: 建表 学生请假申请表
create database if not exists oe_dwd;
use oe_dwd;
DROP TABLE if exists oe_dwd.fac_leave;
create table if not exists oe_dwd.fac_leave
(
    `id`              string  ,
    `class_id`        string            comment '班级id',
    `student_id`      string            comment '学员id',
    `audit_state`     tinyint            comment '审核状态 0 待审核 1 通过 2 不通过',
    `leave_type`      int            comment '请假类型  1 请假 2 销假',
    `leave_reason`    int            comment '请假原因  1 事假 2 病假',
    `begin_time`      string            comment '请假开始时间',
    `begin_time_type` int            comment '1：上午 2：下午',
    `end_time`        string            comment '请假结束时间',
    `end_time_type`   int            comment '1：上午 2：下午',
    `days`            string            comment '请假/已休天数',
    `cancel_state`    tinyint            comment '撤销状态  0 未撤销 1 已撤销',
    `cancel_time`     string            comment '撤销时间',
    `old_leave_id`    string            comment '原请假id，只有leave_type =2 销假的时候才有',
    `valid_state`     tinyint            comment '是否有效（0：无效 1：有效）',
    `create_time`     string            comment '创建时间'
)comment '学生请假申请表' partitioned by(dt string) -- 天分区
row format delimited fields terminated by '\t'
stored as orc tblproperties ('orc.compress'='snappy');

/*
INSERT overwrite TABLE oe_dwd.fac_leave partition(dt)
select id,
       class_id,
       student_id,
       audit_state,
       leave_type,
       leave_reason,
       begin_time,
       begin_time_type,
       end_time,
       end_time_type,
       days,
       cancel_state,
       cancel_time,
       old_leave_id,
       valid_state,
       create_time,
       dt
from oe_ods.t_student_leave_apply;
*/


-- step5.1: ods建表-学生打卡记录表tbh_student_signin_record
DROP TABLE if exists oe_dwd.fac_signin;
create table if not exists oe_dwd.fac_signin
(
    `id`                    string,
    `normal_class_flag`    int comment '是否正常上课 1 正常 2 自习 ',
    `time_table_id`         string comment '作息时间id 关联tbh_school_time_table 或者 tbh_class_time_table',
    `class_id`              string comment '班级id',
    `student_id`            string comment '学员id',
    `signin_time`           string comment '签到时间',
    `signin_date`           string comment '签到日期',
    `signin_type`           string comment '签到类型 1 心跳打卡 2 老师补卡',
    `share_state`           string comment '共享屏幕状态 0 否 1 是 在上午或者下午段有共屏记录，则该段所有记录该字段为1 外网默认为0'

)comment '学生打卡记录表'
partitioned by (dt string)
row format delimited
fields terminated by '\t' --指定分隔符制表符
stored as orc tblproperties ('orc.compress'='snappy');
"
