#! /bin/bash
export LANG=zh_CN.UTF-8
HIVE_HOME=/usr/bin/hive

${HIVE_HOME} -S -e "
create database if not exists oe_dwd;
use oe_dwd;
--客户意向事实表
drop table if exists oe_dwd.fact_customer_relationship;
create table oe_dwd.fact_customer_relationship (
    id                          string comment '意向id',
    update_date_time            string comment '最后更新时间',
    deleted                     string comment '是否被删除（禁用）',
    customer_id                 string comment '所属客户id',
    first_id                    string comment '第一条客户关系id',
    last_visit_time             string comment '最后回访时间',
    next_visit_time             string comment '下次回访时间',
    origin_type                 string comment '数据来源',
    itcast_school_id            string comment '校区Id',
    itcast_subject_id           string comment '学科Id',
    intention_study_type        string comment '意向学习方式',
    anticipat_signup_date       string comment '预计报名时间',
    level                       string comment '客户级别',
    creator                     string comment '创建人',
    current_creator             string comment '当前创建人：初始创建人，当在公海拉回时为 拉回人',
    creator_name                string comment '创建者姓名',
    origin_channel              string comment '来源渠道',
    first_customer_clue_id      string comment '第一条线索id',
    last_customer_clue_id       string comment '最后一条线索id',
    process_state               string comment '处理状态',
    process_time                string comment '处理状态变动时间',
    payment_state               string comment '支付状态',
    payment_time                string comment '支付状态变动时间',
    signup_state                string comment '报名状态',
    signup_time                 string comment '报名时间',
    notice_state                string comment '通知状态',
    notice_time                 string comment '通知状态变动时间',
    lock_state                  string comment '锁定状态',
    lock_time                   string comment '锁定状态修改时间',
    itcast_clazz_id             string comment '所属ems班级id',
    itcast_clazz_time           string comment '报班时间',
    ems_student_id              string comment 'ems的学生id',
    course_id                   string comment '课程ID',
    course_name                 string comment '课程名称',
    appeal_id                   string comment '申诉id',
    total_fee                   string comment '报名费总金额',
    follow_type                 tinyint comment '分配类型，0-自动分配，1-手动分配，2-自动转移，3-手动单个转移，4-手动批量转移，5-公海领取',
   create_date_time            string comment '意向构建时间',--拉链起始时间
    end_date                    string comment '拉链结束时间'
) comment '客户意向事实表'
partitioned by (dt string)  --拉链起始时间 也是表分区字段
row format delimited fields terminated by '\t'
stored as orc tblproperties ('orc.compress' = 'SNAPPY');

--客户线索事实表
drop table if exists oe_dwd.fact_customer_clue;
create table oe_dwd.fact_customer_clue
(
    id                       string comment '线索id',
    create_date_time         string comment '创建时间',
    update_date_time         string comment '最后更新时间',
    deleted                  string comment '是否被删除（禁用）',
    customer_id              string comment '客户id',
    customer_relationship_id string comment '客户关系id',
    sid                      string comment '访客id',
    status                   string comment '状态（undeal待领取 deal 已领取 finish 已关闭 changePeer 已流转）',
    platform                 string comment '平台来源 （pc-网站咨询|wap-wap咨询|sdk-app咨询|weixin-微信咨询）',
    s_name                   string comment '用户名称',
    seo_source               string comment '搜索来源',
    seo_keywords             string comment '关键字',
    referrer                 string comment '上级来源页面',
    from_url                 string comment '会话来源页面',
    landing_page_url         string comment '访客着陆页面',
    url_title                string comment '咨询页面title',
    end_time                 string comment '会话结束时间',
    browser_name             string comment '浏览器名称',
    os_info                  string comment '系统名称',
    area                     string comment '区域',
    country                  string comment '所在国家',
    province                 string comment '省',
    city                     string comment '城市',
    creator                  string comment '创建人',
    name                     string comment '客户姓名',
    phone                    string comment '手机号',
    itcast_school_id         string comment '校区Id',
    itcast_school            string comment '校区',
    itcast_subject_id        string comment '学科Id',
    itcast_subject           string comment '学科',
    wechat                   string comment '微信',
    qq                       string comment 'qq号',
    email                    string comment '邮箱',
    gender                   string comment '性别',
    origin_type              string comment '数据来源渠道',
    information_way          string comment '资讯方式',
    technical_directions     string comment '技术方向',
    valid                    string comment '该线索是否是网资有效线索',
    clue_state               string comment '线索状态',
    scrm_department_id       string comment 'SCRM内部部门id',
    origin_channel           string comment '投放渠道',
    course_id                string,
    course_name              string,
    zhuge_session_id         string,
    is_repeat                tinyint comment '是否重复线索(手机号维度) 0:正常 1：重复'
) comment '客户线索事实表'
partitioned by (dt string)
row format delimited fields terminated by '\t'
stored as orc tblproperties ('orc.compress' = 'SNAPPY');
 "