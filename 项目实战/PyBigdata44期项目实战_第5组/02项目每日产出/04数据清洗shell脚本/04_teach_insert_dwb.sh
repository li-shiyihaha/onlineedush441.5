#! /bin/bash
export LANG=zh_CN.UTF-8
HIVE_HOME=/usr/bin/hive


${HIVE_HOME} -S -e "
--hive压缩
set hive.exec.compress.intermediate=true;
set hive.exec.compress.output=true;
--写入时压缩生效
set hive.exec.orc.compression.strategy=COMPRESSION;

/*select substr('2019-08-19 08:20:00.0',1,10);*/

--打卡主题明细宽表
insert into  hive.oe_dwb.dwb_signin_detail
select
    s.id,
    s.time_table_id,
    s.class_id,
    s.student_id,
    s.signin_time,
    s.signin_date,
    s.share_state,
    u.class_date,
    u.content,
    c.morning_begin_time,
    c.morning_end_time,
    c.afternoon_begin_time,
    c.afternoon_end_time,
    c.evening_begin_time,
    c.evening_end_time,
    s.dt as dt
from
hive.oe_dwd.fac_signin s
    left join hive.oe_dwd.dim_upload u on s.class_id = u.class_id
    left join hive.oe_dwd.dim_class c on s.class_id =c.class_id
 where
       u.class_date = s.signin_date and
       u.class_date <= c.use_end_date and u.class_date >=c.use_begin_date
       and u.class_date is not null;

 select count(*) from hive.oe_dwb.dwb_signin_detail;
-- (减去大于小于条件 )


--请假主题明细宽表
insert into  hive.oe_dwb.dwb_leave_detail
select l.id as id,
       l.class_id as class_id,
       student_id,
       leave_type,
       audit_state,
       begin_time,
       begin_time_type,
       end_time,
       end_time_type,
       valid_state,
       cancel_state,
       class_date,
       content,
       morning_begin_time ,
       morning_end_time,
       afternoon_begin_time,
       afternoon_end_time ,
       evening_begin_time ,
       evening_end_time,
       use_begin_date,
       use_end_date,
       u.dt as dt
from
hive.oe_dwd.fac_leave l
    left join hive.oe_dwd.dim_upload u on l.class_id = u.class_id
    left join hive.oe_dwd.dim_class c on l.class_id =c.class_id;
	"

