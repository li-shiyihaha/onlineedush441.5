#! /bin/bash
export LANG=zh_CN.UTF-8
HIVE_HOME=/usr/bin/hive


${HIVE_HOME} -S -e "
create database if not exists oe_dwb;
use oe_dwb;
--访问咨询信息宽表
drop table if exists oe_dwb.dwb_web_chat_ems_2019_07;
create database if not exists oe_dwb;
use oe_dwb;
create table  oe_dwb.dwb_web_chat_ems_2019_07
(
    id                           string     comment '主键'        ,
    session_id                   string     comment '会话系统sessionId',
    sid                          string     comment '访客id',
    create_time                  string     comment '会话创建时间',
    seo_source                   string     comment '搜索来源',
    ip                           string     comment 'IP地址',
    area                         string     comment '地域',
    country                      string     comment '所在国家',
    origin_channel               string     comment '来源渠道(广告)',
    msg_count                    int        comment '客户发送消息数',
    from_url                     string     comment '会话来源页面'
)
comment '访问咨询信息宽表'
    partitioned by (dt string)
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'ZLIB');
"