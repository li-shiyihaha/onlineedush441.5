#! /bin/bash
export LANG=zh_CN.UTF-8
HIVE_HOME=/usr/bin/hive

${HIVE_HOME} -S -e "
create database if not exists oe_dwd;
use oe_dwd;
---访问咨询信息dim表
drop table if exists oe_dwd.dwd_web_chat_ems_2019_07;
drop table if exists oe_dwd.dwd_dim_web_chat_text_ems_2019_07;
create table         oe_dwd.dwd_dim_web_chat_text_ems_2019_07
(
    id                   string   comment '主键'
    ,from_url             string   comment '会话来源页面'
-- end_date string  comment '拉链结束日期'
)
comment '访问咨询信息副表'
    partitioned by (start_date string)   -- 需要改一下分区 字段
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'ZLIB');
insert into table oe_dwd.dwd_dim_web_chat_text_ems_2019_07 partition (start_date)
select
     id
    ,from_url
    ,dt  as start_date
from oe_ods.t_web_chat_text_ems_2019_07;

-- dwd 访问咨询fact表

drop table if exists oe_dwd.dwd_fact_web_chat_ems_2019_07;
create table  oe_dwd.dwd_fact_web_chat_ems_2019_07
(
    id                           string     comment '主键'        ,
    session_id                   string     comment '会话系统sessionId',
    sid                          string     comment '访客id',
    create_time                  string     comment '会话创建时间',
    seo_source                   string     comment '搜索来源',
    ip                           string     comment 'IP地址',
    area                         string     comment '地域',
    country                      string     comment '所在国家',
    province                     string     comment '省',
    city                         string     comment '城市',
    origin_channel               string     comment '来源渠道(广告)',
    msg_count                    int        comment '客户发送消息数',
    browser_name                 string     comment '浏览网页名字',
    end_date                     string     comment '拉链结束时间'
    )
comment '访问咨询信息主表'
    partitioned by (start_date string)
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'ZLIB');
"