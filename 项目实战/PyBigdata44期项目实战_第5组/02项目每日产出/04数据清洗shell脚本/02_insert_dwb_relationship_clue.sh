#! /bin/bash
export LANG=zh_CN.UTF-8
HIVE_HOME=/usr/bin/hive

${HIVE_HOME} -S -e "
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;
--hive压缩
set hive.exec.compress.intermediate=true;
set hive.exec.compress.output=true;
--写入时压缩生效
set hive.exec.orc.compression.strategy=COMPRESSION;

--意向宽表
insert into oe_dwb.customer_relationship_detail partition (create_date_time)
select re.id                                               as customer_relationship_id,
       re.customer_id,
       cu.name                                             as customer_name,
       re.origin_type,
       re.origin_channel,
       cuc.clue_state,
       re.itcast_school_id                                 as school_id,
       its.name                                            as school_name,
       re.itcast_subject_id                                as subject_id,
       itt.name                                            as subject_name,
       re.creator,
       em.real_name                                        as creator_name,
       em.tdepart_id,
       de.name                                             as tdepart_name,
       if(cu.area_array[0] != '', cu.area_array[0], '\\N') as country,
       if(cu.area_array[1] != '', cu.area_array[1], '\\N') as province,
       if(cu.area_array[2] != '', cu.area_array[2], '\\N') as city,
       re.dt,
       substr(re.create_date_time, 1, 10)                  as create_date_time
from (select customer_id,
             id,
             itcast_school_id,
             itcast_subject_id,
             creator,
             origin_type,
             origin_channel,
             dt,
             create_date_time
      from oe_dwd.fact_customer_relationship
      where end_date = '9999-99-99'
        and substr(create_date_time, 1, 4) = '2011') re    ----按年份从2011年-2016年分段插入
         left join oe_dwd.dim_customer cu on re.customer_id = cu.id
         left join oe_dwd.fact_customer_clue cuc on re.id = cuc.customer_relationship_id
         left join oe_dwd.dim_itcast_school its on re.itcast_school_id = its.id
         left join oe_dwd.dim_itcast_subject itt on re.itcast_subject_id = itt.id
         left join oe_dwd.dim_employee em on re.creator = em.id
         left join oe_dwd.dim_scrm_department de on em.tdepart_id = de.id;

insert into oe_dwb.customer_relationship_detail partition (create_date_time)
select re.id                                               as customer_relationship_id,
       re.customer_id,
       cu.name                                             as customer_name,
       re.origin_type,
       re.origin_channel,
       cuc.clue_state,
       re.itcast_school_id                                 as school_id,
       its.name                                            as school_name,
       re.itcast_subject_id                                as subject_id,
       itt.name                                            as subject_name,
       re.creator,
       em.real_name                                        as creator_name,
       em.tdepart_id,
       de.name                                             as tdepart_name,
       if(cu.area_array[0] != '', cu.area_array[0], '\\N') as country,
       if(cu.area_array[1] != '', cu.area_array[1], '\\N') as province,
       if(cu.area_array[2] != '', cu.area_array[2], '\\N') as city,
       re.dt,
       substr(re.create_date_time, 1, 10)                  as create_date_time
from (select customer_id,
             id,
             itcast_school_id,
             itcast_subject_id,
             creator,
             origin_type,
             origin_channel,
             dt,
             create_date_time
      from oe_dwd.fact_customer_relationship
      where end_date = '9999-99-99'
        and substr(create_date_time, 1, 4) = '2012') re    ----按年份从2011年-2016年分段插入
         left join oe_dwd.dim_customer cu on re.customer_id = cu.id
         left join oe_dwd.fact_customer_clue cuc on re.id = cuc.customer_relationship_id
         left join oe_dwd.dim_itcast_school its on re.itcast_school_id = its.id
         left join oe_dwd.dim_itcast_subject itt on re.itcast_subject_id = itt.id
         left join oe_dwd.dim_employee em on re.creator = em.id
         left join oe_dwd.dim_scrm_department de on em.tdepart_id = de.id;

insert into oe_dwb.customer_relationship_detail partition (create_date_time)
select re.id                                               as customer_relationship_id,
       re.customer_id,
       cu.name                                             as customer_name,
       re.origin_type,
       re.origin_channel,
       cuc.clue_state,
       re.itcast_school_id                                 as school_id,
       its.name                                            as school_name,
       re.itcast_subject_id                                as subject_id,
       itt.name                                            as subject_name,
       re.creator,
       em.real_name                                        as creator_name,
       em.tdepart_id,
       de.name                                             as tdepart_name,
       if(cu.area_array[0] != '', cu.area_array[0], '\\N') as country,
       if(cu.area_array[1] != '', cu.area_array[1], '\\N') as province,
       if(cu.area_array[2] != '', cu.area_array[2], '\\N') as city,
       re.dt,
       substr(re.create_date_time, 1, 10)                  as create_date_time
from (select customer_id,
             id,
             itcast_school_id,
             itcast_subject_id,
             creator,
             origin_type,
             origin_channel,
             dt,
             create_date_time
      from oe_dwd.fact_customer_relationship
      where end_date = '9999-99-99'
        and substr(create_date_time, 1, 4) = '2013') re    ----按年份从2011年-2016年分段插入
         left join oe_dwd.dim_customer cu on re.customer_id = cu.id
         left join oe_dwd.fact_customer_clue cuc on re.id = cuc.customer_relationship_id
         left join oe_dwd.dim_itcast_school its on re.itcast_school_id = its.id
         left join oe_dwd.dim_itcast_subject itt on re.itcast_subject_id = itt.id
         left join oe_dwd.dim_employee em on re.creator = em.id
         left join oe_dwd.dim_scrm_department de on em.tdepart_id = de.id;

insert into oe_dwb.customer_relationship_detail partition (create_date_time)
select re.id                                               as customer_relationship_id,
       re.customer_id,
       cu.name                                             as customer_name,
       re.origin_type,
       re.origin_channel,
       cuc.clue_state,
       re.itcast_school_id                                 as school_id,
       its.name                                            as school_name,
       re.itcast_subject_id                                as subject_id,
       itt.name                                            as subject_name,
       re.creator,
       em.real_name                                        as creator_name,
       em.tdepart_id,
       de.name                                             as tdepart_name,
       if(cu.area_array[0] != '', cu.area_array[0], '\\N') as country,
       if(cu.area_array[1] != '', cu.area_array[1], '\\N') as province,
       if(cu.area_array[2] != '', cu.area_array[2], '\\N') as city,
       re.dt,
       substr(re.create_date_time, 1, 10)                  as create_date_time
from (select customer_id,
             id,
             itcast_school_id,
             itcast_subject_id,
             creator,
             origin_type,
             origin_channel,
             dt,
             create_date_time
      from oe_dwd.fact_customer_relationship
      where end_date = '9999-99-99'
        and substr(create_date_time, 1, 4) = '2014') re    ----按年份从2011年-2016年分段插入
         left join oe_dwd.dim_customer cu on re.customer_id = cu.id
         left join oe_dwd.fact_customer_clue cuc on re.id = cuc.customer_relationship_id
         left join oe_dwd.dim_itcast_school its on re.itcast_school_id = its.id
         left join oe_dwd.dim_itcast_subject itt on re.itcast_subject_id = itt.id
         left join oe_dwd.dim_employee em on re.creator = em.id
         left join oe_dwd.dim_scrm_department de on em.tdepart_id = de.id;

insert into oe_dwb.customer_relationship_detail partition (create_date_time)
select re.id                                               as customer_relationship_id,
       re.customer_id,
       cu.name                                             as customer_name,
       re.origin_type,
       re.origin_channel,
       cuc.clue_state,
       re.itcast_school_id                                 as school_id,
       its.name                                            as school_name,
       re.itcast_subject_id                                as subject_id,
       itt.name                                            as subject_name,
       re.creator,
       em.real_name                                        as creator_name,
       em.tdepart_id,
       de.name                                             as tdepart_name,
       if(cu.area_array[0] != '', cu.area_array[0], '\\N') as country,
       if(cu.area_array[1] != '', cu.area_array[1], '\\N') as province,
       if(cu.area_array[2] != '', cu.area_array[2], '\\N') as city,
       re.dt,
       substr(re.create_date_time, 1, 10)                  as create_date_time
from (select customer_id,
             id,
             itcast_school_id,
             itcast_subject_id,
             creator,
             origin_type,
             origin_channel,
             dt,
             create_date_time
      from oe_dwd.fact_customer_relationship
      where end_date = '9999-99-99'
        and substr(create_date_time, 1, 4) = '2015') re    ----按年份从2011年-2016年分段插入
         left join oe_dwd.dim_customer cu on re.customer_id = cu.id
         left join oe_dwd.fact_customer_clue cuc on re.id = cuc.customer_relationship_id
         left join oe_dwd.dim_itcast_school its on re.itcast_school_id = its.id
         left join oe_dwd.dim_itcast_subject itt on re.itcast_subject_id = itt.id
         left join oe_dwd.dim_employee em on re.creator = em.id
         left join oe_dwd.dim_scrm_department de on em.tdepart_id = de.id;

insert into oe_dwb.customer_relationship_detail partition (create_date_time)
select re.id                                               as customer_relationship_id,
       re.customer_id,
       cu.name                                             as customer_name,
       re.origin_type,
       re.origin_channel,
       cuc.clue_state,
       re.itcast_school_id                                 as school_id,
       its.name                                            as school_name,
       re.itcast_subject_id                                as subject_id,
       itt.name                                            as subject_name,
       re.creator,
       em.real_name                                        as creator_name,
       em.tdepart_id,
       de.name                                             as tdepart_name,
       if(cu.area_array[0] != '', cu.area_array[0], '\\N') as country,
       if(cu.area_array[1] != '', cu.area_array[1], '\\N') as province,
       if(cu.area_array[2] != '', cu.area_array[2], '\\N') as city,
       re.dt,
       substr(re.create_date_time, 1, 10)                  as create_date_time
from (select customer_id,
             id,
             itcast_school_id,
             itcast_subject_id,
             creator,
             origin_type,
             origin_channel,
             dt,
             create_date_time
      from oe_dwd.fact_customer_relationship
      where end_date = '9999-99-99'
        and substr(create_date_time, 1, 4) = '2016') re    ----按年份从2011年-2016年分段插入
         left join oe_dwd.dim_customer cu on re.customer_id = cu.id
         left join oe_dwd.fact_customer_clue cuc on re.id = cuc.customer_relationship_id
         left join oe_dwd.dim_itcast_school its on re.itcast_school_id = its.id
         left join oe_dwd.dim_itcast_subject itt on re.itcast_subject_id = itt.id
         left join oe_dwd.dim_employee em on re.creator = em.id
         left join oe_dwd.dim_scrm_department de on em.tdepart_id = de.id;

--线索宽表
insert into table oe_dwb.customer_clue_detail partition (create_date_time)
select
    fcc.id as clue_id,
    fcc.customer_relationship_id,
    fcc.origin_type,
    fcc.valid,
    fcc.clue_state,
    fcc.is_repeat,
    dca.appeal_status,
    substr(fcc.create_date_time,12,2) as hour,
    fcc.dt,
    substr(fcc.create_date_time,1,10)
from oe_dwd.fact_customer_clue fcc
left join oe_dwd.dim_customer_appeal dca on fcc.customer_relationship_id=dca.customer_relationship_first_id;
  "