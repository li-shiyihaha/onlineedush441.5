#! /bin/bash
export LANG=zh_CN.UTF-8
HIVE_HOME=/usr/bin/hive


${HIVE_HOME} -S -e "


-- step2.2: 全量导入学生请假申请表
INSERT overwrite TABLE oe_dwd.fac_leave partition(dt)
select id,
       class_id,
       student_id,
       audit_state,
       leave_type,
       leave_reason,
       begin_time,
       begin_time_type,
       end_time,
       end_time_type,
       days,
       cancel_state,
       cancel_time,
       old_leave_id,
       valid_state,
       create_time,
       dt
from oe_ods.t_student_leave_apply;

-- step5.2: insert学生打卡记录表

INSERT overwrite TABLE oe_dwd.fac_signin partition(dt)
select id,
       normal_class_flag,
       time_table_id,
       class_id,
       student_id,
       signin_time,
       signin_date,
       signin_type,
       share_state,
       dt
from oe_ods.t_tbh_student_signin_record;
"