
#! /bin/bash
export LANG=zh_CN.UTF-8
HIVE_HOME=/usr/bin/hive


${HIVE_HOME} -S -e "

-- 赵长浩
create database if not exists oe_dwb;
-- dwb_customer_relationship_wide  客户意向宽表
drop table if exists oe_dwb.dwb_customer_relationship_wide;
create table if not exists oe_dwb.dwb_customer_relationship_wide(
    id                          string         comment '意向id',
    create_date_time            string         comment '创建时间',
    origin_type                 string         comment  '数据来源',
    itcast_school_id            string         comment  '校区Id',
    itcast_subject_id           string         comment  '学科Id',
    creator                     string         comment    '创建人',
    creator_name                string         comment   '创建者姓名',
    itcast_clazz_id             string         comment '所属ems班级id',
    origin_channel              string         comment   '来源渠道',
    payment_state               string         comment  '支付状态',
-- 从员工信息表关联得到的字段
    emp_id              string ,
    real_name           string           comment '员工的真实姓名',
    tdepart_id          string           comment '直属部门',
-- 从员工部门表关联得到字段
    depar_id         string    comment '部门id',
    name             string    comment '部门名称',
-- 班级信息表
    ems_id                  string                 comment 'ems课程id(非自增)',
    itcast_school_name      string                 comment 'ems校区名称',
    itcast_subject_name     string                 comment 'ems学科名称',
    itcast_brand            string                 comment 'ems品牌'
)partitioned by(dt string)
row format delimited fields terminated by '\t'
stored as TextFile;
-- 数据同步
-- 因为采用了动态分区插入技术 因此需要设置相关参数
-- 分区
/*SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;
--hive压缩
set hive.exec.compress.intermediate=true;
set hive.exec.compress.output=true;
--写入时压缩生效
set hive.exec.orc.compression.strategy=COMPRESSION;*/
insert overwrite table oe_dwb.dwb_customer_relationship_wide partition (dt)
select
    fcr.id                                 ,
    fcr.create_date_time                   ,
    fcr.origin_type                        ,
    fcr.itcast_school_id                   ,
    fcr.itcast_subject_id                  ,
    fcr.creator                            ,
    fcr.creator_name                       ,
    fcr.itcast_clazz_id                    ,
    fcr.origin_channel                     ,
    fcr.payment_state                      ,
    de.id as emp_id                        ,
    de.real_name                           ,
    de.tdepart_id                          ,
    dsd.id as depar_id                     ,
    dsd.name                               ,
    dic.id as ems_id                       ,
    dic.itcast_school_name                 ,
    dic.itcast_subject_name                ,
    dic.itcast_brand                       ,
    substring(fcr.create_date_time,1,10) as dt
from oe_dwd.fact_customer_relationship fcr
left join oe_dwd.dim_itcast_clazz dic on fcr.itcast_clazz_id=dic.id and fcr.end_date='9999-99-99'
left join oe_dwd.dim_employee de on fcr.creator=de.id
left join oe_dwd.dim_scrm_department dsd on de.tdepart_id=dsd.id;
"


