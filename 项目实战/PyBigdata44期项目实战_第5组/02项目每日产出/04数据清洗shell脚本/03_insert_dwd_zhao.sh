#! /bin/bash
export LANG=zh_CN.UTF-8
HIVE_HOME=/usr/bin/hive


${HIVE_HOME} -S -e "


-- 赵长浩
-- dim_customer_appeal  数据清洗同步
insert overwrite table oe_dwd.dim_customer_appeal partition (dt)
select
    id,
    customer_relationship_first_id,
    employee_id,
    employee_name,
    employee_department_id,
    employee_tdepart_id,
    case appeal_status when 0 then '待稽核'
                       when 1 then '无效'
                       when 2 then '有效'
                       else 'other' end as appeal_status,
    audit_id,
    audit_name,
    audit_department_id,
    audit_department_name,
    audit_date_time,
    create_date_time,
    update_date_time,
    deleted,
    tenant,
    dt
from oe_ods.t_customer_appeal;
-- dim_employee  数据清洗同步
insert overwrite table oe_dwd.dim_employee partition (dt)
select
    id,
    email,
    real_name,
    phone,
    department_id,
    department_name,
    remote_login,
    job_number,
    cross_school,
    last_login_date,
    creator,
    create_date_time,
    update_date_time,
    deleted,
    scrm_department_id,
    leave_office,
    leave_office_time,
    reinstated_time,
    superior_leaders_id,
    tdepart_id,
    tenant,
    ems_user_name,
    dt
from oe_ods.t_employee;
-- dim_itcast_clazz  数据清洗同步
insert overwrite table oe_dwd.dim_itcast_clazz partition (dt)
select
    id,
    create_date_time,
    update_date_time,
    deleted,
    itcast_school_id,
    itcast_school_name,
    itcast_subject_id,
    itcast_subject_name,
    itcast_brand,
    clazz_type_state,
    clazz_type_name,
    teaching_mode,
    start_time,
    end_time,
    comment,
    detail,
    uncertain,
    tenant,
    dt
from oe_ods.t_itcast_clazz;
-- dim_scrm_department  数据清洗同步
insert overwrite table oe_dwd.dim_scrm_department partition (dt)
select
    id,
    name,
    parent_id,
    create_date_time,
    update_date_time,
    deleted,
    id_path,
    tdepart_code,
    creator,
    depart_level,
    depart_sign,
    depart_line,
    depart_sort,
    disable_flag,
    tenant,
    dt
from oe_ods.t_scrm_department;

-- fact_customer_relationship 数据清洗同步(拉链表)
insert overwrite table oe_dwd.fact_customer_relationship partition (start_date)
select
    id,
    create_date_time,
    update_date_time,
    deleted,
    customer_id,
    first_id,
    belonger,
    belonger_name,
    initial_belonger,
    distribution_handler,
    business_scrm_department_id,
    last_visit_time,
    next_visit_time,
    case origin_type when 'NETSERVICE'  then '线上'
                     when  'OTHER'  then  '线下'
                      else 'other'  end  as origin_type,
    itcast_school_id,
    itcast_subject_id,
    intention_study_type,
    anticipat_signup_date,
    level,
    creator,
    current_creator,
    creator_name,
    origin_channel,
    first_customer_clue_id,
    last_customer_clue_id,
    process_state,
    process_time,
    payment_state,
    payment_time,
    signup_state,
    signup_time,
    notice_state,
    notice_time,
    lock_state,
    lock_time,
    itcast_clazz_id,
    itcast_clazz_time,
    payment_url,
    payment_url_time,
    ems_student_id,
    delete_reason,
    deleter,
    deleter_name,
    delete_time,
    course_id,
    course_name,
    delete_comment,
    close_state,
    close_time,
    appeal_id,
    tenant,
    total_fee,
    belonged,
    belonged_time,
    transfer,
    transfer_time,
    follow_type,
    transfer_bxg_oa_account,
    transfer_bxg_belonger_name,
    '9999-99-99' as end_date,
    dt as start_date
from oe_ods.t_customer_relationship;
-- MySQL数据源 插入一条数据，更新一条数据
/*insert into scrm.customer_relationship values
(999999,'2024-01-02 09:07:41','2024-01-02 20:47:31',false,100000,100000,null,null,null,null,43,'2024-01-02 00:00:00',null,'SCHOOL',1,null,null,null,'C',1578,1578,'刘晓曲',"",'毕业学校：安徽科技学院，计划报读班级：null',100000,100000,null,null,null,null,null,null,null,null,false,null,null,null,null,null,null,null,1578,'刘晓曲','2024-01-02 03:12:32',null,null,null,'RELEASE','2024-01-02 03:12:32',null,1,null,1578,null,null,null,null,1,null,null
);
update scrm.customer_relationship set create_date_time='2024-01-02 09:17:41'
where id=100000;
update scrm.customer_relationship set update_date_time='2024-01-03 09:17:41'
where id=100000;
update scrm.customer_relationship set last_visit_time='2024-02-02 09:17:41'
where id=100000;
update scrm.customer_relationship set delete_time='2024-03-02 09:17:41'
where id=100000;
update scrm.customer_relationship set close_time='2024-03-02 09:27:41'
where id=100000;
-- 查询验证MySQL数据源中的数据
select * from scrm.customer_relationship;*/
-- 增量数据采集
/*/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/scrm?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select *, '2024-01-02' as dt from customer_relationship where id is not null and 1=1 and (create_date_time between '2024-01-02 00:00:00' and '2024-01-02 23:59:59') or (update_date_time between '2024-01-02 00:00:00' and '2024-01-02 23:59:59') and \$CONDITIONS" \
--input-null-string '\\N' --input-null-non-string '\\N' \
--hcatalog-database oe_ods \
--hcatalog-table t_customer_relationship \
-m 1*/
-- 拉链表left join 增量表  as 结果集A
insert overwrite table oe_dwd.fact_customer_relationship_tmp partition (start_date)
select
    fcr.id,
    fcr.create_date_time,
    fcr.update_date_time,
    cast(fcr.deleted as string) as deleted,
    fcr.customer_id,
    fcr.first_id,
    fcr.belonger,
    fcr.belonger_name,
    fcr.initial_belonger,
    fcr.distribution_handler,
    fcr.business_scrm_department_id,
    fcr.last_visit_time,
    fcr.next_visit_time,
    fcr.origin_type,
    fcr.itcast_school_id,
    fcr.itcast_subject_id,
    fcr.intention_study_type,
    fcr.anticipat_signup_date,
    fcr.level,
    fcr.creator,
    fcr.current_creator,
    fcr.creator_name,
    fcr.origin_channel,
    fcr.first_customer_clue_id,
    fcr.last_customer_clue_id,
    fcr.process_state,
    fcr.process_time,
    fcr.payment_state,
    fcr.payment_time,
    fcr.signup_state,
    fcr.signup_time,
    fcr.notice_state,
    fcr.notice_time,
    fcr.lock_state,
    fcr.lock_time,
    fcr.itcast_clazz_id,
    fcr.itcast_clazz_time,
    fcr.payment_url,
    fcr.payment_url_time,
    fcr.ems_student_id,
    fcr.delete_reason,
    fcr.deleter,
    fcr.deleter_name,
    fcr.delete_time,
    fcr.course_id,
    fcr.course_name,
    fcr.delete_comment,
    fcr.close_state,
    fcr.close_time,
    fcr.appeal_id,
    fcr.tenant,
    fcr.total_fee,
    fcr.belonged,
    fcr.belonged_time,
    fcr.transfer,
    fcr.transfer_time,
    fcr.follow_type,
    fcr.transfer_bxg_oa_account,
    fcr.transfer_bxg_belonger_name,
    if(tcr.id is not null and fcr.end_date = '9999-99-99',date_add(tcr.dt,-1),fcr.end_date) as end_date,
    fcr.start_date
from oe_dwd.fact_customer_relationship fcr
left join (select * from oe_ods.t_customer_relationship where dt = '2024-01-02') tcr on fcr.id=tcr.id
-- 结果集A union all 增量表
union all
select
id,
    create_date_time,
    update_date_time,
    deleted,
    customer_id,
    first_id,
    belonger,
    belonger_name,
    initial_belonger,
    distribution_handler,
    business_scrm_department_id,
    last_visit_time,
    next_visit_time,
    case origin_type when 'NETSERVICE'  then '线上'
                     when  'OTHER'  then  '线下'
                      else 'other'  end  as origin_type,
    itcast_school_id,
    itcast_subject_id,
    intention_study_type,
    anticipat_signup_date,
    level,
    creator,
    current_creator,
    creator_name,
    origin_channel,
    first_customer_clue_id,
    last_customer_clue_id,
    process_state,
    process_time,
    payment_state,
    payment_time,
    signup_state,
    signup_time,
    notice_state,
    notice_time,
    lock_state,
    lock_time,
    itcast_clazz_id,
    itcast_clazz_time,
    payment_url,
    payment_url_time,
    ems_student_id,
    delete_reason,
    deleter,
    deleter_name,
    delete_time,
    course_id,
    course_name,
    delete_comment,
    close_state,
    close_time,
    appeal_id,
    tenant,
    total_fee,
    belonged,
    belonged_time,
    transfer,
    transfer_time,
    follow_type,
    transfer_bxg_oa_account,
    transfer_bxg_belonger_name,
    '9999-99-99' as end_date,
    '2024-01-02' as start_date
from oe_ods.t_customer_relationship
where dt = '2024-01-02';

-- 结果插入临时表验证
create table if not exists oe_dwd.fact_customer_relationship_tmp (
    id     string,
    create_date_time              string         comment '创建时间',
    update_date_time              string         comment '最后更新时间',
    deleted                       tinyint        comment '是否被删除（禁用)',
    customer_id                   string         comment '所属客户id',
    first_id  
"