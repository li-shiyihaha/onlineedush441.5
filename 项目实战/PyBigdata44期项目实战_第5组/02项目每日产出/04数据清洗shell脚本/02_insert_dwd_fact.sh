#! /bin/bash
export LANG=zh_CN.UTF-8
HIVE_HOME=/usr/bin/hive

${HIVE_HOME} -S -e "
------------------因为采用了动态分区插入技术 因此需要设置相关参数---------------
--分区
SET hive.exec.dynamic.partition=true;
SET hive.exec.dynamic.partition.mode=nonstrict;
set hive.exec.max.dynamic.partitions.pernode=10000;
set hive.exec.max.dynamic.partitions=100000;
set hive.exec.max.created.files=150000;
--hive压缩
set hive.exec.compress.intermediate=true;
set hive.exec.compress.output=true;
--写入时压缩生效
set hive.exec.orc.compression.strategy=COMPRESSION;

--客户意向事实表
insert overwrite table oe_dwd.fact_customer_relationship partition (dt)
select id,
       update_date_time,
       deleted,           --合法性判断的字段
       customer_id,
       first_id,
       last_visit_time,
       next_visit_time,
       origin_type,       --线上线下
       itcast_school_id,
       itcast_subject_id, --意向学科id
       intention_study_type,
       anticipat_signup_date,
       level,
       creator,
       current_creator,
       creator_name,
       origin_channel,
       first_customer_clue_id,
       last_customer_clue_id,
       process_state,
       process_time,
       payment_state,
       payment_time,
       signup_state,
       signup_time,
       notice_state,
       notice_time,
       lock_state,
       lock_time,
       itcast_clazz_id,
       itcast_clazz_time,
       ems_student_id,
       course_id,
       course_name,
       appeal_id,
       total_fee,
       follow_type,
       create_date_time,
       '9999-99-99' as end_date,
       dt
from oe_ods.t_customer_relationship
where customer_id is not null
  and customer_id != ''
  and origin_type is not null
  and origin_type != ''
  and create_date_time is not null
  and create_date_time != ''
  and deleted = 'false';

----客户线索事实表
insert overwrite table oe_dwd.fact_customer_clue partition (dt)
select id,
       create_date_time,
       update_date_time,
       deleted,
       customer_id,
       customer_relationship_id,
       sid,
       status,
       platform,
       s_name,
       seo_source,
       seo_keywords,
       referrer,
       from_url,
       landing_page_url,
       url_title,
       end_time,
       browser_name,
       os_info,
       area,
       country,
       province,
       city,
       creator,
       name,
       phone,
       itcast_school_id,
       itcast_school,
       itcast_subject_id,
       itcast_subject,
       wechat,
       qq,
       email,
       gender,
       origin_type,
       information_way,
       technical_directions,
       valid,
       clue_state,
       scrm_department_id,
       origin_channel,
       course_id,
       course_name,
       zhuge_session_id,
       is_repeat,
       dt
from oe_ods.t_customer_clue
where customer_relationship_id is not null
  and customer_relationship_id != ''
  and clue_state is not null
  and clue_state != ''
  and deleted = 'false';
  "