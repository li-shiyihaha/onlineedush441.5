#! /bin/bash
export LANG=zh_CN.UTF-8
HIVE_HOME=/usr/bin/hive


${HIVE_HOME} -S -e "

create database if not exists oe_dwb;
use oe_dwb;

-- 打卡主题明细宽表
DROP TABLE if EXISTS oe_dwb.dwb_signin_detail;
CREATE TABLE oe_dwb.dwb_signin_detail(
    -- 打卡
        `id`                    string,
    `time_table_id`         string comment '作息时间id 关联tbh_school_time_table ',
    `class_id`              string comment '班级id',
    `student_id`            string comment '学员id',
    `signin_time`           string comment '签到时间',
    `signin_date`           string comment '签到日期',
    `share_state`           string comment '共享屏幕状态 0 否 1 是 在上午或者下午段有共屏记录，则该段所有记录该字段为1 外网默认为0',
    -- 排课信息
    `class_date`            string  comment '上课日期',
     `content`                string comment '课程内容【null代表休息，或者是开班典礼 =》 无效打卡或者无效请假】',
    -- 班级作息时间
    `morning_begin_time`    string comment '上午开始时间',
    `morning_end_time`      string comment '上午结束时间',
    `afternoon_begin_time`  string comment '下午开始时间',
    `afternoon_end_time`    string comment '下午结束时间',
    `evening_begin_time`    string comment '晚上开始时间',
    `evening_end_time`      string comment '晚上结束时间'
    )
COMMENT '打卡主题明细宽表'
PARTITIONED BY(dt STRING)
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');


-- 请假主题明细宽表(leave_type=1筛选请假类型)
DROP TABLE if EXISTS oe_dwb.dwb_leave_detail;
CREATE TABLE oe_dwb.dwb_leave_detail(
   -- 请假主题
        `id`          string,
    `class_id`        string            comment '班级id',
    `student_id`      string            comment '学员id',
    `leave_type`      int                comment '请假类型  1 请假 2 销假',
    `audit_state`     tinyint            comment '审核状态 0 待审核 1 通过 2 不通过',
    `begin_time`      string            comment '请假开始时间',
    `begin_time_type` int              comment '1：上午 2：下午',
    `end_time`        string            comment '请假结束时间',
    `end_time_type`   int              comment '1：上午 2：下午',
    `valid_state`     tinyint            comment '是否有效（0：无效 1：有效）',
     `cancel_state`    tinyint          comment '撤销状态  0 未撤销 1 已撤销',
    -- 排课信息
    `class_date`            string  comment '上课日期',
     `content`                string comment '课程内容【null代表休息，或者是开班典礼 =》 无效打卡或者无效请假】',
    -- 班级作息时间
    `morning_begin_time`    string comment '上午开始时间',
    `morning_end_time`      string comment '上午结束时间',
    `afternoon_begin_time`  string comment '下午开始时间',
    `afternoon_end_time`    string comment '下午结束时间',
    `evening_begin_time`    string comment '晚上开始时间',
    `evening_end_time`      string comment '晚上结束时间',
    `use_begin_date`    string comment '作息生效时间',
    `use_end_date`      string comment '生效结束时间'
    )
COMMENT '请假主题明细宽表'
PARTITIONED BY(dt STRING)
row format delimited fields terminated by '\t'
stored as orc
tblproperties ('orc.compress' = 'SNAPPY');
"