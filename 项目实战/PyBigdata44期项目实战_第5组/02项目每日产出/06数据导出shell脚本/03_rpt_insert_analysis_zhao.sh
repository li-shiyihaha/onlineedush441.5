#! /bin/bash
export LANG=zh_CN.UTF-8
PRESTO_HOME=/export/server/presto/bin/presto


${PRESTO_HOME} --catalog hive --server 192.168.88.80:8090 --execute "


-- 赵长浩

-- 校区报名柱状图   全部报名客户中，各校区报名人数分布
insert into hive.oe_rpt.rpt_school_cnt
select
       b.itcast_school_name as itcast_school_name,
       count(*) as school_cnt
from hive.oe_ods.t_customer_relationship a left join hive.oe_ods.t_itcast_clazz b on a.itcast_clazz_id=b.id
where a.payment_state = 'PAID' and b.itcast_school_name is not null
group by b.itcast_school_name
order by school_cnt desc;
-- 学科报名柱状图    全部报名客户中，各学科报名人数分布
insert into hive.oe_rpt.rpt_subject_cnt
select
       b.itcast_subject_name as itcast_subject_name,
       count(*) as subject_cnt
from hive.oe_ods.t_customer_relationship a left join hive.oe_ods.t_itcast_clazz b on a.itcast_clazz_id=b.id
where a.payment_state = 'PAID' and b.itcast_subject_name is not null
group by b.itcast_subject_name
order by subject_cnt desc;
-- 总报名量   已经缴费的报名客户总量
insert into hive.oe_rpt.rpt_total_cnt
select count(*) as total_cnt
from hive.oe_ods.t_customer_relationship
where payment_state = 'PAID';
--  线上报名量  总报名量中来源渠道为线上访客的报名总量
insert into hive.oe_rpt.rpt_online_cnt
select count(*) as online_cnt
from hive.oe_ods.t_customer_relationship
where payment_state = 'PAID' and origin_type = 'NETSERVICE';
--  意向客户报名转化率  全部报名人数/全部新增的意向人数
insert into hive.oe_rpt.rpt_total_rate
with t1 as (select count(*) as cnt1
from hive.oe_ods.t_customer_relationship
where payment_state = 'PAID'),
t2 as (select count(*) as cnt2
from hive.oe_ods.t_customer_clue
where cast(msg_count as bigint) > 0)
select cast(cast(t1.cnt1 as decimal(38,4))/cast(t2.cnt2 as decimal(38,4)) as decimal(38,2)) as total_rate
from t1,t2;
-- 有效线索报名转化率  线上报名量/线上有效线索量
insert into hive.oe_rpt.rpt_online_rate
with t1 as (select count(*) as online_cnt
from hive.oe_ods.t_customer_relationship
where payment_state = 'PAID' and origin_type = 'NETSERVICE'),
     t2 as (with a as (select count(*) as cnt1 from hive.oe_dwd.fact_customer_clue where valid = 'true' and is_repeat='0'),
            b as (select count(*) as cnt2
        from hive.oe_dwd.fact_customer_clue fcc left join hive.oe_dwd.dim_customer_appeal dca on fcc.customer_relationship_id=dca.customer_relationship_first_id
        where fcc.valid = 'false' and dca.appeal_status != 1)
        select (a.cnt1+b.cnt2) as online_clue_cnt
        from a,b)
select cast(cast(t1.online_cnt as decimal(38,4))/cast(t2.online_clue_cnt as decimal(38,4)) as decimal(38,2)) as online_rate
from t1,t2;
-- 日报名趋势图   每天报名人数的趋势图
insert into hive.oe_rpt.rpt_day_cnt
select
       substring (create_date_time,1,10)  as day_code,
       count(*) as day_cnt
from hive.oe_ods.t_customer_relationship
where payment_state = 'PAID'
group by create_date_time
order by create_date_time desc;

-- 来源渠道占比  全部报名学员中，不同来源渠道的报名学员占比情况
insert into hive.oe_rpt.rpt_channel_rate
with t1 as (select
        origin_channel,
        count(*) as channel_cnt
    from hive.oe_ods.t_customer_relationship
    where payment_state = 'PAID'
    group by origin_channel),
     t2 as (select count(*) as total_cnt
from hive.oe_ods.t_customer_relationship
where payment_state = 'PAID')
select
       t1.origin_channel as origin_channel,
       cast(cast(t1.channel_cnt as decimal(38,4))/cast(t2.total_cnt as decimal(38,4)) as decimal(38,2)) as channel_rate
from t1,t2;
-- 咨询中心报名贡献   全部报名学员中，各咨询中心的报名学员人数占比情况
insert into hive.oe_rpt.rpt_center_rate
with t1 as (select
        name,
        count(*) as center_cnt
    from hive.oe_dwb.dwb_customer_relationship_wide
    where payment_state = 'PAID' and name is not null
    group by name
    order by center_cnt desc),
     t2 as (select count(*) as total_cnt
from hive.oe_ods.t_customer_relationship
where payment_state = 'PAID')
select
       t1.name  as  name ,
       cast(cast(t1.center_cnt as decimal(38,4))/cast(t2.total_cnt as decimal(38,4)) as decimal(38,2)) as center_rate
from t1,t2;
-- 校区学科报名人数topN
insert into hive.oe_rpt.rpt_sch_sub_cnt
select
       b.itcast_school_name as itcast_school_name,
       b.itcast_subject_name as itcast_subject_name,
       count(*) as sch_sub_cnt
from hive.oe_ods.t_customer_relationship a left join hive.oe_ods.t_itcast_clazz b on a.itcast_clazz_id=b.id
where a.payment_state = 'PAID' and b.itcast_school_name is not null
group by b.itcast_school_name,b.itcast_subject_name
order by sch_sub_cnt desc;"