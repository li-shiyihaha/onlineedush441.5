#! /bin/bash
export LANG=zh_CN.UTF-8
PRESTO_HOME=/opt/cloudera/parcels/presto/bin/presto
MYSQL_HOME=/usr/bin/mysql


echo '========================================'
echo '================开始导出================='
echo '========================================'


${MYSQL_HOME} -h172.17.0.203 -p3306 -uroot -p123456 -Dyipin_20210903 -e "
-- step1: 在presto中配置mysql的connector
-- todo step2: 使用ctas语法实现导出
/* sql 执行
drop table if exists oe_olap.rpt_final_daycount; */

/*
    使用presto
    将hive rpt数据导出到mysql中
 */

create table mysql.oe_olap.rpt_final_daycount as
select class_date,
       class_id,
       stu_cnt,
       time_type,
       att_morning_cnt,
       att_morning_rate,
       att_afternoon_cnt,
       att_afternoon_rate,
       att_evening_cnt,
       att_evening_rate,
       late_morning_cnt,
       late_morning_rate,
       late_afternoon_cnt,
       late_afternoon_rate,
       late_evening_cnt,
       late_evening_rate,
coalesce (leave_morning_cnt,0)as leave_morning_cnt,
coalesce (leave_morning_rate,0)as leave_morning_rate,
coalesce (leave_afternoon_cnt,0)as leave_afternoon_cnt,
coalesce (leave_afternoon_rate,0)as leave_afternoon_rate,
coalesce (leave_evening_cnt,0)as leave_evening_cnt,
coalesce (leave_evening_rate,0)as leave_evening_rate,
coalesce (absent_morning_cnt,0)as absent_morning_cnt,
coalesce (absent_morning_rate,0)as absent_morning_rate,
coalesce (absent_afternoon_cnt,0) as absent_afternoon_cnt,
coalesce (absent_afternoon_rate,0 ) as absent_afternoon_rate,
coalesce (absent_evening_cnt,0) as absent_evening_cnt,
coalesce (absent_evening_rate,0) as absent_evening_rate
from oe_dm.dm_final_daycount;
"

echo '========================================'
echo '=================success================'
echo '========================================'
