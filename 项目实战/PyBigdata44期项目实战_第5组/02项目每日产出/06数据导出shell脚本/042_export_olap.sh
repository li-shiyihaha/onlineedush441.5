
#! /bin/bash
export LANG=zh_CN.UTF-8
PRESTO_HOME=/opt/cloudera/parcels/presto/bin/presto
MYSQL_HOME=/usr/bin/mysql


echo '========================================'
echo '================开始导出================='
echo '========================================'


${MYSQL_HOME} -h172.17.0.203 -p3306 -uroot -p123456 -Dyipin_20210903 -e "

-- sql 插入。drop table if exists oe_olap.rpt_cnt_yang
-- presto插入  ;
/**/
create table mysql.oe_olap.rpt_cnt_yang as

select class_date,class_id,stu_cnt,att_morning_cnt   as cnt,att_morning_rate as rate,    '出勤' as sign_type,   'morning' as time_type  from mysql.oe_olap.rpt_final_daycount
union all
select class_date,class_id,stu_cnt,att_afternoon_cnt as cnt,att_afternoon_rate as rate,  '出勤' as sign_type,   'afternoon' as time_type  from mysql.oe_olap.rpt_final_daycount
union all
select class_date,class_id,stu_cnt,att_evening_cnt as cnt,att_evening_rate as rate,      '出勤' as sign_type,   'evening' as time_type  from mysql.oe_olap.rpt_final_daycount
union all
select class_date,class_id,stu_cnt,late_morning_cnt as cnt,late_morning_rate as rate,    '迟到' as sign_type,    'morning' as time_type  from mysql.oe_olap.rpt_final_daycount
union all
select class_date,class_id,stu_cnt,late_afternoon_cnt as cnt,late_afternoon_rate as rate,'迟到' as sign_type,    'afternoon' as time_type  from mysql.oe_olap.rpt_final_daycount
union all
select class_date,class_id,stu_cnt,late_evening_cnt as cnt,late_evening_rate as rate,    '迟到' as sign_type,    'evening' as time_type  from mysql.oe_olap.rpt_final_daycount
union all
select class_date,class_id,stu_cnt,leave_morning_cnt as   cnt,leave_morning_rate as rate,  '请假' as sign_type,  'morning' as time_type  from mysql.oe_olap.rpt_final_daycount
union all
select class_date,class_id,stu_cnt,leave_afternoon_cnt as cnt,leave_afternoon_rate as rate,'请假' as sign_type,  'afternoon' as time_type  from mysql.oe_olap.rpt_final_daycount
union all
select class_date,class_id,stu_cnt,leave_evening_cnt as   cnt,leave_evening_rate as rate,  '请假' as sign_type,  'evening' as time_type  from mysql.oe_olap.rpt_final_daycount
union all
select class_date,class_id,stu_cnt,absent_morning_cnt as   cnt,absent_morning_rate as rate,  '旷课' as sign_type, 'morning' as time_type  from mysql.oe_olap.rpt_final_daycount
union all
select class_date,class_id,stu_cnt,absent_afternoon_cnt as cnt,absent_afternoon_rate as rate,'旷课' as sign_type, 'afternoon' as time_type  from mysql.oe_olap.rpt_final_daycount
union all
select class_date,class_id,stu_cnt,absent_evening_cnt as   cnt,absent_evening_rate as rate,  '旷课' as sign_type, 'evening' as time_type  from mysql.oe_olap.rpt_final_daycount
order by class_id ;

select * from mysql.oe_olap.rpt_cnt_yang;

"

echo '========================================'
echo '=================success================'
echo '========================================'