#!/bin/bash
/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/scrm?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select *, '2024-01-01' as dt from customer where id is not null and customer_relationship_id is not null and 1=1 and  \$CONDITIONS" \
--input-null-string '\\N' --input-null-non-string '\\N' \
--hcatalog-database oe_ods \
--hcatalog-table t_customer \
-m 1


/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/scrm?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select *, '2024-01-01' as dt from customer_appeal where id is not null and customer_relationship_first_id is not null and 1=1 and \$CONDITIONS" \
--input-null-string '\\N' --input-null-non-string '\\N' \
--hcatalog-database oe_ods \
--hcatalog-table t_customer_appeal \
-m 1


/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/scrm?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select *, '2024-01-01' as dt from customer_clue  where id is not null and customer_relationship_id is not null and 1=1 and \$CONDITIONS" \
--input-null-string '\\N' --input-null-non-string '\\N' \
--hcatalog-database oe_ods \
--hcatalog-table t_customer_clue \
-m 1


/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/scrm?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select *, '2024-01-01' as dt from customer_relationship where id is not null and 1=1 and \$CONDITIONS" \
--input-null-string '\\N' --input-null-non-string '\\N' \
--hcatalog-database oe_ods \
--hcatalog-table t_customer_relationship \
-m 1


/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/scrm?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select *, '2024-01-01' as dt from employee where id is not null and 1=1 and \$CONDITIONS" \
--input-null-string '\\N' --input-null-non-string '\\N' \
--hcatalog-database oe_ods \
--hcatalog-table t_employee \
-m 1


/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/scrm?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select *, '2024-01-01' as dt from itcast_clazz where id is not null and 1=1 and \$CONDITIONS" \
--input-null-string '\\N' --input-null-non-string '\\N' \
--hcatalog-database oe_ods \
--hcatalog-table t_itcast_clazz \
-m 1


/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/scrm?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select *, '2024-01-01' as dt from itcast_school where id is not null and 1=1 and \$CONDITIONS" \
--input-null-string '\\N' --input-null-non-string '\\N' \
--hcatalog-database oe_ods \
--hcatalog-table t_itcast_school \
-m 1


/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/scrm?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select *, '2024-01-01' as dt from itcast_subject where id is not null and 1=1 and \$CONDITIONS" \
--input-null-string '\\N' --input-null-non-string '\\N' \
--hcatalog-database oe_ods \
--hcatalog-table t_itcast_subject \
-m 1


/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/scrm?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select *, '2024-01-01' as dt from scrm_department where id is not null and 1=1 and \$CONDITIONS" \
--input-null-string '\\N' --input-null-non-string '\\N' \
--hcatalog-database oe_ods \
--hcatalog-table t_scrm_department \
-m 1
