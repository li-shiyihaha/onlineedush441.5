#! /bin/bash
#SQOOP_HOME=/opt/cloudera/parcels/CDH-6.2.1-1.cdh6.2.1.p0.1425774/bin/sqoop
export LANG=zh_CN.UTF-8
SQOOP_HOME=/usr/bin/sqoop
if [[ $1 == "" ]];then
   TD_DATE=`date -d '1 days ago' "+%Y-%m-%d"`
else
   TD_DATE=$1
fi

echo '========================================'
echo '==============开始全量导入==============='
echo '========================================'

/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/teach?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select *, '2024-01-01' as dt from class_studying_student_count where id is not null  and 1=1 and  \$CONDITIONS" \
--input-null-string '\\N' --input-null-non-string '\\N' \
--hcatalog-database oe_ods \
--hcatalog-table t_class_studying_student_count \
-m 1

/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/teach?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select *, '2024-01-01' as dt from student_leave_apply where id is not null and class_id is not null and 1=1 and  \$CONDITIONS" \
--input-null-string '\\N' --input-null-non-string '\\N' \
--hcatalog-database oe_ods \
--hcatalog-table t_student_leave_apply \
-m 1

/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/teach?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select *, '2024-01-01' as dt from course_table_upload_detail where class_id is not null and 1=1 and  \$CONDITIONS" \
--input-null-string '\\N' --input-null-non-string '\\N' \
--hcatalog-database oe_ods \
--hcatalog-table t_course_table_upload_detail \
-m 1

/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/teach?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select *, '2024-01-01' as dt from tbh_class_time_table where id is not null and class_id is not null and 1=1 and  \$CONDITIONS" \
--input-null-string '\\N' --input-null-non-string '\\N' \
--hcatalog-database oe_ods \
--hcatalog-table t_tbh_class_time_table \
-m 1

/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/teach?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select *, '2024-01-01' as dt from tbh_student_signin_record where id is not null and time_table_id is not null and 1=1 and  \$CONDITIONS" \
--input-null-string '\\N' --input-null-non-string '\\N' \
--hcatalog-database oe_ods \
--hcatalog-table t_tbh_student_signin_record \
-m 1

sqoop import \
--connect jdbc:mysql://192.168.88.80:3306/teach \
--username root \
--password 123456 \
--table calendar \
--fields-terminated-by '\t' \
--hcatalog-database oe_ods \
--hcatalog-table t_calendar \
-m 1

wait

echo '========================================'
echo '=================success==============='
echo '========================================'