#!/bin/bash
/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/nev?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select *, '2024-01-01' as dt from web_chat_ems_2019_07 where id is not null and 1=1 and  \$CONDITIONS" \
--input-null-string '\\N' --input-null-non-string '\\N' \
--hcatalog-database oe_ods \
--hcatalog-table t_web_chat_ems_2019_07  \
-m 1

/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/nev?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select *, '2024-01-01' as dt from web_chat_text_ems_2019_07 where id is not null and 1=1 and  \$CONDITIONS" \
--input-null-string '\\N' --input-null-non-string '\\N' \
--hcatalog-database oe_ods  \
--hcatalog-table t_web_chat_text_ems_2019_07 \
-m 1


/usr/bin/sqoop import "-Dorg.apache.sqoop.splitter.allow_text_splitter=true" \
--connect 'jdbc:mysql://192.168.88.80:3306/nev?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true' \
--username root \
--password 123456 \
--query "select *, '2024-01-02' as dt from web_chat_ems_2019_07 where 1=1 and create_date_time between '2024-01-02 00:00:00' and '2024-01-02 23:59:59' or  create_time between '2019-07-03 00:00:00' and '2019-07-03 23:59:59' and  \$CONDITIONS" \
--hcatalog-database oe_ods \
--hcatalog-table t_web_chat_ems_2019_07 \
-m 1