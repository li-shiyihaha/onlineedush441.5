#! /bin/bash
export LANG_zh.UTC-8
HIVE_HOME=/usr/bin/hive

${HIVE_HOME} -S -e "


create database if not exists oe_dm;
use oe_dm;
drop table if exists oe_dm.dm_web_chat_ems_2019_07;
create table oe_dm.dm_web_chat_ems_2019_07
(                                                   --时间
    date_time            string    comment '当日统计日期',
--时间维度
    year_code            string    comment '年',
    quarter_code         string    comment '季度',
    month_code           string    comment '月',
    date_code            string    comment '天',

    area                 string         comment 'area',
    seo_source           string         comment '搜索来源',
    origin_channel       string         comment '来源渠道(广告)',
    from_url             string         comment '来源页面',
--指标计算
    sid_cnt              bigint            comment '访问用户量',
    ip_cnt               bigint            comment 'ip地址个数',
    session_cnt          bigint            comment '会话个数',
    msg_num              bigint            comment '咨询人数',
--分组字段
    time_type            string         comment '时间类型',
    group_type_new       string         comment '分组类型'

)comment '访问咨询信息表'
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');



"