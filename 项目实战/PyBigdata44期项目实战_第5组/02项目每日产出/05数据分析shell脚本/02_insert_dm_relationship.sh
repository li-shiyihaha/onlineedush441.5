#!/bin/bash
export LANG=zh CN.UTF-8
PRESTO HOME=/export/server/presto/bin/presto

${PRESTO_HOME} --catalog hive --server 192.168.88.80:8090 --execute"

insert into hive.oe_dm.dm_relationship
with temp1 as (select                        --temp1
create_date_time,
substr(create_date_time, 1, 4) as year_code,
substr(create_date_time, 1, 7) as year_month,
substr(create_date_time, 6, 2) as month_code,
substr(create_date_time, 9, 2) as day_month_num,
create_date_time  as dim_date_id,
origin_type ,
clue_state,
group_type,
country,
province,
city,
subject_id,
subject_name,
school_id,
school_name,
origin_channel,
tdepart_id,
tdepart_name,
relationship_cnt
from hive.oe_dws.relationship_daycount),
temp2 as (select                             --temp2
-- 统计日期
'2024-01-01'          as date_time,
--时间标记   year,month,day
case
    when grouping(year_code, month_code, day_month_num, dim_date_id) = 0
    then 'date'
    when grouping(year_code, month_code, year_month) = 0
    then 'month'
    when grouping(year_code) = 0
    then 'year'
    else 'other' end  as time_type,
--时间粒度
year_code,     -- '年code 2014',
year_month,    -- '年月 201401',
month_code,    -- '月份编码 01',
day_month_num, -- '一月第几天 01',
dim_date_id,   -- '日期  20140101',
--旧维度标记
group_type  as group_type_old,
--维度标记
case
    when grouping(country) = 0
        then 'country'
    when grouping(province) = 0
        then 'province'
    when grouping(city) = 0
        then 'city'
    when grouping(subject_id, subject_name) = 0
        then 'subject'
    when grouping(school_id, school_name) = 0
        then 'school'
    when grouping(origin_channel) = 0
        then 'origin_channel'
    when grouping(tdepart_id, tdepart_name) = 0
        then 'tdepart'
    else 'all' end  as group_type_new,
--维度粒度
origin_type,
clue_state,
country,
province,
city,
subject_id,
subject_name,
school_id,
school_name,
origin_channel,
tdepart_id,
tdepart_name,
--统计值
sum(relationship_cnt) as relationship_amt
from temp1
group by
grouping sets (
--年
(year_code, origin_type, clue_state, group_type),                                                          --年+线上线下+新老学员
(year_code, origin_type, clue_state, country, group_type),                                                 --年+线上线下+新老学员+国家
(year_code, origin_type, clue_state, province, group_type),                                                --年+线上线下+新老学员+省份
(year_code, origin_type, clue_state, city, group_type),--年+线上线下+新老学员+城市
(year_code, origin_type, clue_state, subject_id, subject_name, group_type),                                --年+线上线下+新老学员+学科
(year_code, origin_type, clue_state, school_id, school_name, group_type),                                  --年+线上线下+新老学员+校区
(year_code, origin_type, clue_state, origin_channel, group_type),                                          --年+线上线下+新老学员+数据渠道来源
(year_code, origin_type, clue_state, tdepart_id, tdepart_name, group_type),                                --年+线上线下+新老学员+咨询中心
--月
(year_code, year_month, month_code, origin_type, clue_state, group_type),                                  --月+线上线下+新老学员
(year_code, year_month, month_code, origin_type, clue_state, country, group_type),                         --月+线上线下+新老学员+国家
(year_code, year_month, month_code, origin_type, clue_state, province, group_type),                        --月+线上线下+新老学员+省份
(year_code, year_month, month_code, origin_type, clue_state, city, group_type),                            --月+线上线下+新老学员+城市
(year_code, year_month, month_code, origin_type, clue_state, subject_id, subject_name, group_type),        --月+线上线下+新老学员+学科
(year_code, year_month, month_code, origin_type, clue_state, school_id, school_name, group_type),          --月+线上线下+新老学员+校区
(year_code, year_month, month_code, origin_type, clue_state, origin_channel, group_type),                  --月+线上线下+新老学员+数据渠道来源
(year_code, year_month, month_code, origin_type, clue_state, tdepart_id, tdepart_name, group_type),        --月+线上线下+新老学员+咨询中心
--天
(year_code, dim_date_id, day_month_num, year_month, month_code, origin_type, clue_state, group_type),      --月+线上线下+新老学员
(year_code, dim_date_id, day_month_num, year_month, month_code, origin_type, clue_state, country,group_type),--月+线上线下+新老学员+国家
(year_code, dim_date_id, day_month_num, year_month, month_code, origin_type, clue_state, province,group_type),--月+线上线下+新老学员+省份
(year_code, dim_date_id, day_month_num, year_month, month_code, origin_type, clue_state, city, group_type),--月+线上线下+新老学员+城市
(year_code, dim_date_id, day_month_num, year_month, month_code, origin_type, clue_state, subject_id, subject_name,group_type),--月+线上线下+新老学员+学科
(year_code, dim_date_id, day_month_num, year_month, month_code, origin_type, clue_state, school_id, school_name,group_type),--月+线上线下+新老学员+校区
(year_code, dim_date_id, day_month_num, year_month, month_code, origin_type, clue_state, origin_channel,group_type),--月+线上线下+新老学员+数据渠道来源
(year_code, dim_date_id, day_month_num, year_month, month_code, origin_type, clue_state, tdepart_id, tdepart_name,group_type)))--月+线上线下+新老学员+咨询中心
select date_time,
       time_type,
       year_code,
       year_month,
       month_code,
       day_month_num,
       dim_date_id,
       group_type_new as group_type,
       origin_type,
       clue_state,
       country,
       province,
       city,
       subject_id,
       subject_name,
       school_id,
       school_name,
       origin_channel,
       tdepart_id,
       tdepart_name,
       relationship_amt
from temp2
where group_type_old = group_type_new;
"