#! /bin/bash
export LANG=zh_CN.UTF-8
HIVE_HOME=/usr/bin/hive

${HIVE_HOME} -S -e "

create database if not exists oe_dm;
use oe_dm;

-- 意向统计上卷宽表
drop table if exists oe_dm.dm_relationship;
create table if not exists oe_dm.dm_relationship
(
    --统计日期
    date_time        string,
    --时间标记
    time_type        string,
    --时间粒度
    year_code        string, -- '年code 2014',
    year_month       string, -- '年月 201401',
    month_code       string, -- '月份编码 01',
    day_month_num    string, -- '一月第几天 01',
    dim_date_id      string, -- '日期  20140101',
    --维度标记
    group_type       string,
    --维度粒度
    origin_type      string,
    clue_state       string,
    country          string,
    province         string,
    city             string,
    subject_id       string,
    subject_name     string,
    school_id        string,
    school_name      string,
    origin_channel   string,
    tdepart_id       string,
    tdepart_name     string,
    --统计
    relationship_cnt bigint
)
    comment '意向统计上卷宽表'
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');


--线索统计上卷宽表
/*drop table if exists oe_dm.dm_clue;
create table if not exists oe_dm.dm_clue
(
    --统计日期
    date_time     string,
    --时间标记
    time_type     string,
    --时间粒度
    year_code     string, -- '年code 2014',
    year_month    string, -- '年月 201401',
    month_code    string, -- '月份编码 01',
    day_month_num string, -- '一月第几天 01',
    dim_date_id   string, -- '日期  20140101',
    --维度标记
    group_type    string,
    --维度粒度
    origin_type   string,
    clue_state    string,
    --统计
    clu_cnt       bigint,
    clu_rate      decimal(38, 4)
)
    comment '线索统计上卷宽表'
    row format delimited fields terminated by '\t'
    stored as orc tblproperties ('orc.compress' = 'SNAPPY');*/
"