#!/bin/bash
export LANG=zh CN.UTF-8
PRESTO HOME=/export/server/presto/bin/presto

${PRESTO_HOME} --catalog hive --server 192.168.88.80:8090 --execute"

--线索主题日统计
insert into hive.oe_dws.clue_daycount
with temp as (select
--判断
valid,                                 --是否为有效线索 ,ture为有效
appeal_status,                         --判断是否-- 有效 '申诉状态，0:待稽核 1:无效 2：有效',
is_repeat,                             --是否为重复线索 0:正常 1：重复
case when origin_type = 'NETSERVICE' or origin_type = 'PRESIGNUP' then 'online'
else 'offline' end as origin_type,     --数据来源，线上线下
case when clue_state = 'VALID_NEW_CLUES' then 'new'
else 'old' end     as clue_state,      --新老学员
--统计计算
clue_id,                               --线索id
--维度
create_date_time,                      --时间，天
hour,                                  --每小时
row_number() over (partition by clue_id) as rn
from hive.oe_dwb.customer_clue_detail
)
select
--维度
create_date_time,
hour,
case when origin_type = 'online' then 'online'
else 'offline' end as origin_type,
case when clue_state = 'new' then 'new'
else 'old' end as clue_state,
--group_type
case when grouping (hour)=0 then 'hour'
     when grouping (create_date_time) =0 then 'day'
     else null end as group_type,
--指标
--每天线上线下新老用户有效线索个数
case
    when grouping(create_date_time)=0
    then count(if((rn=1 and is_repeat='0' and valid='true') or (rn=1 and is_repeat= '0' and valid='false' and appeal_status!='1'),clue_id,null))
    else null end as clu_count,

--每小时线上线下新老用户有效线索转换率
case
    when grouping(hour)=0
    then cast(cast(count(if((rn=1 and is_repeat= '0' and valid='true') or (rn=1 and is_repeat= '0' and valid='false' and appeal_status!='1'),clue_id,null)) as DECIMAL(38,4))/cast(count(if(rn=1 and is_repeat= '0',clue_id,null)) as DECIMAL(38,4)) * 100 as DECIMAL(5,2))
    else null end as clu_rate
from temp
group by
grouping sets(
(hour,origin_type,clue_state),
(create_date_time,origin_type,clue_state)
);
"