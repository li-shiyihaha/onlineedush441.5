#! /bin/bash
export LANG=zh_CN.UTF-8
HIVE_HOME=/usr/bin/hive


${HIVE_HOME} -S -e "

create database  if not exists oe_dws;
use oe_dws;

drop table if exists oe_dws.dws_signin_daycount;
create table if not exists  oe_dws.dws_signin_daycount(
    class_date string comment '上课日期',
    class_id string comment '班级id',
    stu_cnt string comment '班级总人数',
    time_type string comment '时间段信息 用case when分',
    att_morning_cnt bigint comment '上午出勤人数',
    att_morning_rate decimal(38,2) comment '上午出勤率',
    att_afternoon_cnt bigint comment '下午出勤人数',
    att_afternoon_rate decimal(38,2) comment '下午出勤率',
    att_evening_cnt bigint comment '晚上出勤人数',
    att_evening_rate decimal(38,2) comment '晚上出勤率',
    late_morning_cnt bigint comment '上午迟到人数',
    late_morning_rate decimal(38,2) comment '上午迟到率',
    late_afternoon_cnt bigint comment '下午迟到人数',
    late_afternoon_rate decimal(38,2) comment '下午迟到率',
    late_evening_cnt bigint comment '晚上迟到人数',
    late_evening_rate decimal(38,2) comment '晚上迟到率',
    leave_morning_cnt bigint comment '上午请假人数',
    leave_morning_rate decimal(38,2) comment '上午请假率',
    leave_afternoon_cnt bigint comment '下午请假人数',
    leave_afternoon_rate decimal(38,2) comment '下午请假率',
    leave_evening_cnt bigint comment '晚上请假人数',
    leave_evening_rate decimal(38,2) comment '晚上请假率',
    absent_morning_cnt bigint comment '上午旷课人数',
    absent_morning_rate decimal(38,2) comment '上午旷课率',
    absent_afternoon_cnt bigint comment '下午旷课人数',
    absent_afternoon_rate decimal(38,2) comment '下午旷课率',
    absent_evening_cnt bigint comment '晚上旷课人数',
    absent_evening_rate decimal(38,2) comment '晚上旷课率'
) comment '数据日统计表'
row format delimited
fields terminated by '\t'
stored as orc
tblproperties ('orc.compress'='snappy')
;
"
