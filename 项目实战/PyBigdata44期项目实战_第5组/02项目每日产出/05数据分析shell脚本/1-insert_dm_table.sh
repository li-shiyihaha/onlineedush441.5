#! /bin/bash
export LANG=zh_CN.UTF-8
PRESTO_HOME=/export/server/presto/bin/presto


${PRESTO_HOME} --catalog hive --server 192.168.88.80:8090 --execute "
insert into hive.oe_dm.dm_web_chat_ems_2019_07
with dws_time_tmp as(
select
    '2024-05-05'  as date_time,
        --时间维度
       substring(date_code,1,4) as  year_code,
       case when ceil(cast (substring(date_code,6,2) as int)/ 3) = 1 then '第一季度'
            when ceil(cast(substring(date_code,6,2)as int) / 3) = 2 then '第二季度'
            when ceil(cast(substring(date_code,6,2)as int) / 3) = 3 then '第三季度'
            when ceil(cast(substring(date_code,6,2)as int) / 3) = 4 then '第四季度'
            else null end as quarter_code        ,
       substring(date_code,1,7) as   month_code  ,
       date_code,
       group_type,
 --维度
       area                       ,
       seo_source                  ,
       origin_channel              ,
       from_url                    ,
       sid_cnt                     ,
       ip_cnt                      ,
       session_cnt                 ,
       msg_num
      from oe_dws.dws_web_chat_ems_2019_07_daycount),
dws_tmp as(
     select
      '2024-05-05'  as date_time,
        --时间维度
       year_code,
       quarter_code,
       month_code,
       date_code,
       group_type          as   group_type_old,
       area                       ,
       seo_source                  ,
       origin_channel              ,
       from_url                    ,
       sum(sid_cnt )       as   sid_cnt     ,
       sum(ip_cnt   )      as   ip_cnt      ,
       sum(session_cnt  )  as   session_cnt ,
       sum(msg_num)        as   msg_num,

       --if(sum(if(sid_cnt>0,1,0)) =0 , null, msg_num / sid_cnt  * 100 as msg_rate)
        --cast(msg_num / sid_cnt as decimal(5,2))* 100 as msg_rate)
    case
        when grouping(from_url) = 0 then '来源页面'
        when grouping(seo_source) = 0 then '搜索来源'
        when grouping(origin_channel) = 0 then '来源渠道'
        when grouping(area) = 0 then 'area'
        when grouping(date_code) = 0 then 'all'
        else 'other' end as group_type_new,
    --年统计的8个维度
    case
        when grouping(year_code,quarter_code,month_code,date_code ) = 0 then '天'
        when grouping(year_code,quarter_code,month_code ) = 0 then '月'
        when grouping(year_code,quarter_code ) = 0 then '季度'
        when grouping(year_code ) = 0 then '年'
        else 'other' end as time_type
from dws_time_tmp
group by
    grouping sets
    (
--年
      (year_code, group_type),
      (year_code, area,group_type),
      (year_code, from_url,group_type),
      (year_code, seo_source,group_type),
      (year_code, origin_channel,group_type),
--季
      (year_code,quarter_code,group_type),
      (year_code,quarter_code, area,group_type),
      (year_code,quarter_code, from_url,group_type),
      (year_code,quarter_code, seo_source,group_type),
      (year_code,quarter_code, origin_channel,group_type),
--月
      (year_code,quarter_code,month_code, group_type),
      (year_code,quarter_code,month_code, area,group_type),
      (year_code,quarter_code,month_code, from_url,group_type),
      (year_code,quarter_code,month_code, seo_source,group_type),
      (year_code,quarter_code,month_code, origin_channel,group_type),
--天
      (year_code,quarter_code,month_code,date_code,group_type),
      (year_code,quarter_code,month_code,date_code, area,group_type),
      (year_code,quarter_code,month_code,date_code, from_url,group_type),
      (year_code,quarter_code,month_code,date_code, seo_source,group_type),
      (year_code,quarter_code,month_code,date_code, origin_channel,group_type))
     )
select
date_time,
year_code,
quarter_code,
month_code,
date_code,
area,
seo_source,
origin_channel,
from_url,
sid_cnt,
ip_cnt,
session_cnt,
msg_num,
time_type,
group_type_new
from dws_tmp
where group_type_old = group_type_new;     --再执行这个条件;

"