#! /bin/bash
export LANG=zh_CN.UTF-8
HIVE_HOME=/usr/bin/hive

${HIVE_HOME} -S -e "

create database if not exists oe_dws;
use oe_dws;

--意向主题日统计
drop table if exists oe_dws.relationship_daycount;
create table oe_dws.relationship_daycount
(
--维度
    create_date_time   string, --天
    origin_type        string, --线上线下
    clue_state         string, --新老学员
    country            string, --国家地区
    province           string, --省份地区
    city               string, --城市地区
    subject_id         string, --学科id
    subject_name       string, --学科名
    school_id          string, --校区id
    school_name        string, --校区名
    origin_channel     string, --数据来源渠道
    tdepart_id         string, --部门id
    tdepart_name       string, --部门名
--维度标记
    group_type string,
--日统计
    relationship_cnt  bigint
) comment '意向题日统计'
row format delimited fields terminated by '\t'
stored as orc tblproperties ('orc.compress' = 'SNAPPY');


-- 线索主题日统计
drop table if exists oe_dws.clue_daycount;
create table oe_dws.clue_daycount
(
--维度
    create_date_time   string, --时间
    hour               string,--小时
    origin_type        string, --线上线下
    clue_state         string, --新老学员
--维度标记
    group_type string,
--日统计
    clu_cnt            bigint,  --线索个数
    clu_rate           DECIMAL(38,4) --线索转换率
) comment '线索主题日统计'
row format delimited fields terminated by '\t'
stored as orc tblproperties ('orc.compress' = 'SNAPPY');
  "