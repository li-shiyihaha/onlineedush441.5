
#! /bin/bash
export LANG=zh_CN.UTF-8
PRESTO_HOME=/export/server/presto/bin/presto


${PRESTO_HOME} --catalog hive --server 192.168.88.80:8090 --execute "
insert into hive.oe_dm.dm_final_daycount
select class_date,
       class_id,
       stu_cnt,
       time_type,
       att_morning_cnt,
       att_morning_rate,
       att_afternoon_cnt,
       att_afternoon_rate,
       att_evening_cnt,
       att_evening_rate,
       late_morning_cnt,
       late_morning_rate,
       late_afternoon_cnt,
       late_afternoon_rate,
       late_evening_cnt,
       late_evening_rate,
       leave_morning_cnt,
       leave_morning_rate,
       leave_afternoon_cnt,
       leave_afternoon_rate,
       leave_evening_cnt,
       leave_evening_rate,
       absent_morning_cnt,
       absent_morning_rate,
       absent_afternoon_cnt,
       absent_afternoon_rate,
       absent_evening_cnt,
       absent_evening_rate
from oe_dws.dws_signin_daycount;

select * from hive.oe_dws.dws_signin_daycount;
 "